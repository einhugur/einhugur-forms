﻿using System;
using Foundation;

namespace Einhugur.Threading
{
    public class Timer : IDisposable
    {
        NSTimer timer;
        double interval;
        bool repeats;

        public event EventHandler Action;

        public Timer()
        {
            timer = null;
            repeats = true;
            interval = 1.0;
        }

        public double Interval
        {
            get => interval;
            set => interval = value;
        }

        public bool Repeats
        {
            get => repeats;
            set => repeats = value;
        }

        public bool IsRunning => timer != null && timer.IsValid;

        public void Start()
        {
            if (timer != null)
            {
                throw new ApplicationException("Timer already running");
            }
            timer = NSTimer.CreateTimer(interval, repeats, OnAction);

            NSRunLoop.Main.AddTimer(timer, NSRunLoopMode.Default);
        }

        protected void OnAction(NSTimer sender)
        {
            Action?.Invoke(this, EventArgs.Empty);
        }

        public void Stop()
        {
            timer?.Invalidate();
        }

        public void Dispose()
        {
            Action = null;
            timer?.Dispose();
        }
    }
}

