﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;

namespace Einhugur
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    public class FileSystemItem
    {
        readonly string path;

        public FileSystemItem(string path)
        {
            this.path = path;
        }

        public static FileSystemItem CurrentDirectory
        {
            get => new FileSystemItem(Directory.GetCurrentDirectory());
            set => Directory.SetCurrentDirectory(value.FullPath);
        }

        public static FileSystemItem GetTempFile() => new FileSystemItem(Path.GetTempFileName());

        public string FullPath => path;

        public string Name => IsDirectory ?
            Path.GetDirectoryName(path) :
            Path.GetFileName(path);

        public string NameWithoutExtension => IsDirectory ?
            Path.GetDirectoryName(path) :
            Path.GetFileNameWithoutExtension(path);

        public string Extension => IsDirectory ?
            "" :
            Path.GetExtension(path);

        public bool IsDirectory
        {
            get
            {
                var attr = File.GetAttributes(path);

                return attr.HasFlag(FileAttributes.Directory);
            }
        }

        public bool Exists => File.Exists(path) || Directory.Exists(path);

        public void CreateDirectory()
        {
            Directory.CreateDirectory(path);
        }

        public FileSystemItem Child(string name)
        {
            return new FileSystemItem(Path.Combine(path, name));
        }

        public FileSystemItem Parent
        {
            get
            {
                if(String.IsNullOrEmpty(path))
                {
                    return null;
                }

                var parent = Directory.GetParent(path);
                return parent != null ? new FileSystemItem(parent.ToString()) : null;
            }
        }
        
        private static void CopyAll(DirectoryInfo source, DirectoryInfo target)
        {
            Directory.CreateDirectory(target.FullName);

            // Copy each file into the new directory.
            foreach (FileInfo fi in source.GetFiles())
            {
                fi.CopyTo(Path.Combine(target.FullName, fi.Name), true);
            }

            // Copy each subdirectory using recursion.
            foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
            {
                DirectoryInfo nextTargetSubDir =
                    target.CreateSubdirectory(diSourceSubDir.Name);
                CopyAll(diSourceSubDir, nextTargetSubDir);
            }
        }

        public void MoveTo(FileSystemItem destination)
        {
            if (destination == null) throw new ArgumentNullException(); 
            if (!destination.IsDirectory) throw new ArgumentException("destination must be directory");
            
            var destItem = destination.Child(Name);

            if (destItem.Exists)
            {
                throw new ArgumentException("Destination file already exists");
            }
            
            if (IsDirectory)
            {
                Directory.Move(path, destItem.FullPath);
            }
            else
            {
                File.Move(path, destItem.FullPath);
            }
        }

        public void CopyTo(FileSystemItem destination)
        {
            if (destination == null) throw new ArgumentNullException(); 
            if (!destination.IsDirectory) throw new ArgumentException("destination must be directory");

            var destItem = destination.Child(Name);

            if (destItem.Exists)
            {
                throw new ArgumentException("Destination file already exists");
            }
            
            if (IsDirectory)
            {
                CopyAll(new DirectoryInfo(path), new DirectoryInfo(destItem.FullPath));
            }
            else
            {
                File.Copy(path, destItem.FullPath);
            }
        }

        public void Delete()
        {
            if(Exists)
            {
                if(IsDirectory)
                {
                    Directory.Delete(path);
                }
                else
                {
                    File.Delete(path);
                }
            }
        }

        private bool CheckAttribute(FileAttributes attribute)
        {
            if (IsDirectory)
            {
                DirectoryInfo info = new DirectoryInfo(path);

                return ((info.Attributes & attribute) == attribute);
            }
            else
            {
                FileAttributes attributes = File.GetAttributes(path);

                return ((attributes & attribute) == attribute);
            }
        }

        public bool Hidden => CheckAttribute(FileAttributes.Hidden);
        public bool IsReadOnly => CheckAttribute(FileAttributes.ReadOnly);
    }
}

