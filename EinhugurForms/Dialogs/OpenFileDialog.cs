﻿using System.Diagnostics.CodeAnalysis;
using AppKit;

namespace Einhugur.Dialogs
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    public class OpenFileDialog : SaveFileDialog
    {
        public new static FileSystemItem Show(string[] fileTypes)
        {
            var dlg = new OpenFileDialog();

            if (fileTypes != null)
            {
                dlg.AllowedFileTypes = fileTypes;
            }

            if (dlg.Show())
            {
                return dlg.Url;
            }

            return null;
        }

        public OpenFileDialog()
        {
            panel = new NSOpenPanel()
            {
                CanChooseFiles = true
            };
        }
    }
}

