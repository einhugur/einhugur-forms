﻿using System.Diagnostics.CodeAnalysis;
using AppKit;

namespace Einhugur.Dialogs
{
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    public class SelectDirectoryDialog : SaveFileDialog
    {
        public static FileSystemItem Show(string message)
        {
            var dlg = new SelectDirectoryDialog
            {
                Message = message
            };

            if (dlg.Show())
            {
                return dlg.Url;
            }

            return null;
        }
        
        public SelectDirectoryDialog()
        {
            panel = new NSOpenPanel()
            {
                CanChooseFiles = false,
                CanChooseDirectories = true,
                CanCreateDirectories = true
            };
        }

        public bool CanCreateDirectories
        {
            get => panel.CanCreateDirectories;
            set => panel.CanCreateDirectories = value;
        }
    }
}