using System;
using AppKit;
using CoreGraphics;

namespace Einhugur.Forms
{
    public abstract partial class Window
    {
        private class FrameworkViewController : NSViewController
        {
            readonly Action<NSView> viewDidLoad;
            readonly CGRect initialBounds;
            private readonly bool mouseTracking;
            private Window owner;
            

            public FrameworkViewController(CGRect bounds, Action<NSView> viewDidLoad, Window owner, bool mouseTracking)
                
            {
                this.viewDidLoad = viewDidLoad;
                this.initialBounds = bounds;
                this.owner = owner;
                this.mouseTracking = mouseTracking;
            }

            public override void LoadView()
            {
                this.View = new NSViewEx(initialBounds, owner)
                {
                    mouseTracking = mouseTracking
                };

            }



            public override void ViewDidLoad()
            {
                base.ViewDidLoad();
                
                viewDidLoad(View);
            }

            public override void ViewDidDisappear()
            {
                base.ViewDidDisappear();

                owner = null;
            }


        }
    }
}