﻿using System;
using AppKit;
using CoreGraphics;
using Einhugur.Arguments;
using Einhugur.Drawing;
using Foundation;

namespace Einhugur.Forms
{
    public enum CheckBoxState
    {
        Unchecked = 0,
        Checked = 1,
        Mixed = -1
    }
    
    public class Checkbox : Control
    {
        public const int DefaultHeight = 20;
        public const int DefaultWidth = 100;

        public event EventHandler GotFocus;
        public event EventHandler LostFocus;
        public event EventHandler<ActionEventArgs> Action;

        public Checkbox(float x, float y)  : this(x,y, DefaultWidth, DefaultHeight )
        { }

        public Checkbox(float x, float y, float width, float height)
        {
            Handle = new Button.NSButtonEx(new CoreGraphics.CGRect(x, y, width, height), OnGotFocus, OnLostFocus);

            ((NSButton)Handle).SetButtonType(NSButtonType.Switch);

            ((NSButton)Handle).Activated += OnUserAction;
        }

        public string Caption
        {
            get => ((NSButton)Handle).Title;
            set => ((NSButton)Handle).Title = value;
        }

        public bool Enabled
        {
            get => ((NSButton)Handle).Enabled;
            set => ((NSButton)Handle).Enabled = value;
        }
        
        public string ToolTip
        {
            get => ((NSButton) Handle).ToolTip;
            set => ((NSButton) Handle).ToolTip = value;
        }

        public CheckBoxState Value
        {
            get => (CheckBoxState)((NSButton)Handle).State;
            set
            {
                if ((NSCellStateValue)value != ((NSButton)Handle).State)
                {
                    ((NSButton)Handle).State = (NSCellStateValue)value;
                    OnAction(false);
                }
            }
        }

        public bool Checked
        {
            get => ((NSButton)Handle).State == NSCellStateValue.On || ((NSButton)Handle).State == NSCellStateValue.Mixed;
            set
            {
                if (value != Checked)
                {
                    ((NSButton)Handle).State = value ? NSCellStateValue.On : NSCellStateValue.Off;
                    OnAction(false);
                }
            }
        }

        public bool AllowsMixedState
        {
            get => ((NSButton)Handle).AllowsMixedState;
            set => ((NSButton)Handle).AllowsMixedState = value;
        }

        protected virtual void OnAction(bool invokedByUser)
        {
            Action?.Invoke(this, new ActionEventArgs(invokedByUser));
        }

        private void OnUserAction(object sender, EventArgs e)
        {
            OnAction(true);
        }

        protected virtual void OnGotFocus()
        {
            GotFocus?.Invoke(this, EventArgs.Empty);
        }

        protected virtual void OnLostFocus()
        {
            LostFocus?.Invoke(this, EventArgs.Empty);
        }
        
        public override void DrawInto(GraphicsContext g, float x, float y)
        {
            NSButton btn = new NSButton(new CGRect(0, 0, this.Width, this.Height));
            btn.WantsLayer = true;
            btn.SetButtonType(NSButtonType.Switch);
            btn.Title = this.Caption;
            btn.State = ((NSButton) Handle).State;
            btn.Enabled = this.Enabled;
            
            g.Handle.SaveGraphicsState();
            var tr = new NSAffineTransform();
            tr.Translate(x, y);
            tr.Concat();
            
            //btn.DisplayRect(new CGRect(0, 0, this.Width, this.Height) );
            btn.DisplayRectIgnoringOpacity(new CGRect(0, 0, this.Width, this.Height), g.Handle);

            g.Handle.RestoreGraphicsState();
        }

        public override void Dispose()
        {
            ((NSButton)Handle).Activated -= OnUserAction;
            Action = null;
            GotFocus = null;
            LostFocus = null;

            base.Dispose();
        }
    }
}

