﻿using System;
using System.Collections.Generic;
using System.Linq;
using AppKit;
using Foundation;
using static Einhugur.Forms.TabControl;

namespace Einhugur.Forms
{
    public class TabControl : ContainerControl
    {
        public event EventHandler TabSelected;

        public class Tab
        {
            readonly NSTabViewItem item;
            Func<IEnumerable<Control>> setupControls;

            public Tab(Func<IEnumerable<Control>> setupControls = null)
            {
                item = new NSTabViewItem();
                this.setupControls = setupControls;

                item.View = new ContentViewEx();
                               
            }

            public string Caption
            {
                get => item.Label;
                set => item.Label = value;
            }

            internal NSTabViewItem Item => item;

            internal Func<IEnumerable<Control>> SetupControlsProc => setupControls;

            internal void ClearSetupProc()
            {
                setupControls = null;
            }
        }

        private class TabDelegate : NSTabViewDelegate
        {
            readonly WeakReference<TabControl> tabControl;

            public TabDelegate(TabControl tabControl)
            {
                this.tabControl = new WeakReference<TabControl>(tabControl);
            }

            [Export("tabView:didSelectTabViewItem:")]
            public override void DidSelect(NSTabView tabView, NSTabViewItem item)
            {
                if(tabControl.TryGetTarget(out TabControl ctrl))
                {
                    ctrl.OnTabSelected();
                }
            }
        }

       

        public TabControl(float x, float y, float width, float height, Func<IEnumerable<Tab>> setupTabs = null)
        {
            Handle = new NSTabView(new CoreGraphics.CGRect(x, y, width, height));
            ((NSTabView)Handle).Delegate = new TabDelegate(this);

            if (setupTabs != null)
            {
                var tabs = setupTabs();

                if(tabs != null)
                {
                    int i = 0;

                    foreach (var tab in tabs)
                    {
                        i++;
                        ((NSTabView)Handle).Add(tab.Item);

                        
                        if (tab.SetupControlsProc != null)
                        {
                            // We need to get valid size for all the tabs for layout logic to work.
                            if(i != 1)
                            {
                                tab.Item.View.Frame = tabs.First().Item.View.Frame;
                            }

                            SetupControlsForView(tab.Item.View, tab.SetupControlsProc);
                            
                            tab.ClearSetupProc();
                        }
                    }
                }
            }

        }

        public int SelectedTabIndex => Array.FindIndex(((NSTabView)Handle).Items, x => x.Equals(((NSTabView)Handle).Selected));

        protected virtual void OnTabSelected()
        {
            TabSelected?.Invoke(this, EventArgs.Empty);
        }

        public void SelectTab(int index)
        {
            ((NSTabView)Handle).SelectAt(index);
        }

        public void AddTab(TabControl.Tab tab)
        {
            ((NSTabView)Handle).Add(tab.Item);
        }

        public override void Dispose()
        {
            TabSelected = null;

            base.Dispose();
        }
    }
}

