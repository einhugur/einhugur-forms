using System.Diagnostics.CodeAnalysis;
using AppKit;
using Einhugur.Drawing;

namespace Einhugur.Forms
{
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    public class LevelIndicator : Control
    {
        public enum LevelIndicatorStyle : ulong
        {
            Relevancy = NSLevelIndicatorStyle.Relevancy,
            ContinuousCapacity = NSLevelIndicatorStyle.ContinuousCapacity,
            DiscreteCapacity = NSLevelIndicatorStyle.DiscreteCapacity,
            RatingLevel = NSLevelIndicatorStyle.RatingLevel
        }
        
        public enum TickmarkPositioning : ulong
        {
            Above = NSTickMarkPosition.Above,
            Below = NSTickMarkPosition.Below,
            Leading = NSTickMarkPosition.Leading,
            Trailing = NSTickMarkPosition.Trailing,
            Left = NSTickMarkPosition.Left,
            Right = NSTickMarkPosition.Right
        }

        public const int DefaultHeight = 20;
        public const int DefaultWidth = 100;

        public LevelIndicator(float x, float y) : this(x, y, DefaultWidth, DefaultHeight)
        { }

        public LevelIndicator(float x, float y, float width, float height)
        {
            Handle = new NSLevelIndicator(new CoreGraphics.CGRect(x, y, width, height));
            
            
        }

        public double TickmarkValueAt(int tickmarkIndex) => ((NSLevelIndicator) Handle).TickMarkValueAt(tickmarkIndex);

        public int TickmarkCount
        {
            get => (int)((NSLevelIndicator) Handle).TickMarkCount;
            set => ((NSLevelIndicator) Handle).TickMarkCount = value;
        }
        
        public int MajorTickMarkCount
        {
            get => (int)((NSLevelIndicator) Handle).MajorTickMarkCount;
            set => ((NSLevelIndicator) Handle).MajorTickMarkCount = value;
        }
        
        public TickmarkPositioning TickmarkPosition
        {
            get => (TickmarkPositioning)((NSLevelIndicator) Handle).TickMarkPosition;
            set => ((NSLevelIndicator) Handle).TickMarkPosition = (NSTickMarkPosition)value;
        }
        
        public bool DrawsTieredCapacityLevels
        {
            get => ((NSLevelIndicator) Handle).DrawsTieredCapacityLevels;
            set => ((NSLevelIndicator) Handle).DrawsTieredCapacityLevels = value;
        }
        
        public Color FillColor
        {
            get => new Color(((NSLevelIndicator) Handle).FillColor.CGColor);
            set => ((NSLevelIndicator) Handle).FillColor = value.AsNSColor;
        }
        
        public Color CriticalFillColor
        {
            get => new Color(((NSLevelIndicator) Handle).CriticalFillColor.CGColor);
            set => ((NSLevelIndicator) Handle).CriticalFillColor = value.AsNSColor;
        }
        
        public Color WarningFillColor
        {
            get => new Color(((NSLevelIndicator) Handle).WarningFillColor.CGColor);
            set => ((NSLevelIndicator) Handle).WarningFillColor = value.AsNSColor;
        }

        public LevelIndicatorStyle Style
        {
            get => (LevelIndicatorStyle)((NSLevelIndicator) Handle).LevelIndicatorStyle;
            set => ((NSLevelIndicator) Handle).LevelIndicatorStyle = (NSLevelIndicatorStyle)value;
        }
        
        public double MinValue
        {
            get => ((NSLevelIndicator) Handle).MinValue;
            set => ((NSLevelIndicator) Handle).MinValue = value;
        }
        
        public double MaxValue
        {
            get => ((NSLevelIndicator) Handle).MaxValue;
            set => ((NSLevelIndicator) Handle).MaxValue = value;
        }
        
        public double CriticalValue
        {
            get => ((NSLevelIndicator) Handle).CriticalValue;
            set => ((NSLevelIndicator) Handle).CriticalValue = value;
        }
        
        public double WarningValue
        {
            get => ((NSLevelIndicator) Handle).WarningValue;
            set => ((NSLevelIndicator) Handle).WarningValue = value;
        }
        
        public double Value
        {
            get => ((NSLevelIndicator) Handle).DoubleValue;
            set => ((NSLevelIndicator) Handle).DoubleValue = value;
        }
    }
}