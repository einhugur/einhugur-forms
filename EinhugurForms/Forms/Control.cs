﻿using System;
using System.ComponentModel;
using AppKit;
using AudioToolbox;
using CoreGraphics;
using Einhugur.Drawing;
using Foundation;
using static Einhugur.Forms.ContainerControl;

namespace Einhugur.Forms
{
    public abstract class Control : IDisposable
    {
        public event EventHandler Opening;
        public event EventHandler Closing;

        public bool LockRight;
        public bool LockLeft;
        public bool LockTop;
        public bool LockBottom;

        public NSView Handle;
        WeakReference<Window> window;
        WeakReference<Control> parent;

        public Window Window => window == null ? null : window.TryGetTarget(out Window target) ? target : null;

        /// <summary>
        /// For internal framework use, do not try to call this method. 
        /// </summary>
        /// <param name="parentWindow"></param>
        [EditorBrowsable(EditorBrowsableState.Never)]
        internal virtual void SetParentWindow(Window parentWindow)
        {
            this.window = new WeakReference<Window>(parentWindow);

            Opening?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>
        /// For internal framework use, do not try to call this method. 
        /// </summary>
        /// <param name="parentControl"></param>
        [EditorBrowsable(EditorBrowsableState.Never)]
        internal virtual void SetParentControl(Control parentControl)
        {
            this.parent = new WeakReference<Control>(parentControl);
        }

        public Control Parent
        {
            get
            {
                if (parent != null)
                {
                    if (parent.TryGetTarget(out Control control))
                    {
                        return control;
                    }
                }
                return null;
            }
        }

        public nfloat Left
        {
            get => Handle.Frame.Left;
            set
            {
                Handle.Frame = new CoreGraphics.CGRect(value, Handle.Frame.Top, Handle.Frame.Width, Handle.Frame.Height);
                Handle.NeedsDisplay = true;
            }
        }
        
        public nfloat Top
        {
            get => Handle.Frame.Top;
            set
            {
                Handle.Frame = new CoreGraphics.CGRect(
                    Handle.Frame.Left,
                    value,
                    Handle.Frame.Width,
                    Handle.Frame.Height);

                Handle.NeedsDisplay = true;
            }
        }

        public nfloat Width
        {
            get => Handle.Frame.Width;
            set
            {
                Handle.Frame = new CoreGraphics.CGRect(Handle.Frame.Left, Handle.Frame.Top, value, Handle.Frame.Height);
                Handle.NeedsDisplay = true;
            }
        }

        public nfloat Height
        {
            get => Handle.Frame.Height;
            set
            {
                Handle.Frame = new CoreGraphics.CGRect(Handle.Frame.Left, Handle.Frame.Top, Handle.Frame.Width, value);
                Handle.NeedsDisplay = true;
            }
        }

        public nfloat Right
        {
            get => Handle.Frame.Left + Handle.Frame.Width;
        }

        public nfloat Bottom
        {
            get => Handle.Frame.Top + Handle.Frame.Height;
        }

        public virtual void SetFocus()
        {
            Handle.BecomeFirstResponder();
        }

        public bool Visible
        {
            get => !Handle.Hidden;
            set => Handle.Hidden = !value;
        }

        internal virtual void Deactivate()
        {
            
        }

        internal virtual void Activate()
        {
            
        }
        
        public virtual void DrawInto(GraphicsContext g, float x, float y)
        {
            g.Handle.SaveGraphicsState();
            var tr = new NSAffineTransform();
            tr.Translate(x, y);
            tr.Concat();
            
            //Handle.DisplayRect(new CGRect(0, 0, this.Width, this.Height) );
            Handle.DisplayRectIgnoringOpacity(new CGRect(0, 0, this.Width, this.Height), g.Handle);

            g.Handle.RestoreGraphicsState();
            
            /*
              // Alternative way:
            var rep = this.Handle.BitmapImageRepForCachingDisplayInRect(this.Handle.Bounds);
            this.Handle.CacheDisplay(this.Handle.Bounds, rep);
            

            var img = new NSImage(this.Handle.Bounds.Size);
            img.Flipped = true;
            img.AddRepresentation(rep);

            var old = NSGraphicsContext.CurrentContext;
            NSGraphicsContext.CurrentContext = g.Handle;
            img.Draw(new CGPoint(x, y), CGRect.Empty, NSCompositingOperation.Copy, 1.0f);
            NSGraphicsContext.CurrentContext = old;
             */
        }

        public void Close()
        {
            if (Handle.Superview != null)
            {
                if(Handle.Superview is Window.NSViewEx)
                {
                    var owner = ((Window.NSViewEx)Handle.Superview).Owner;

                    if(owner != null)
                    {
                        owner.RemoveControl(this);
                    }
                }
                else 
                {
                    var control = Parent;

                    if(control != null && control is ContainerControl)
                    {
                        ((ContainerControl)control).RemoveControl(this);
                    }
                }

                Handle.RemoveFromSuperview();
            }
        }

        public void Refresh()
        {
            // basically mark our NSView as needing a redraw
            Handle.NeedsDisplay = true;
        }

        public virtual void Dispose()
        {
            Closing?.Invoke(this, EventArgs.Empty);

            Opening = null;
            Closing = null;

            Handle.Dispose();
        }
    }
}

