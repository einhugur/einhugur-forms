﻿using System;
using System.Diagnostics.CodeAnalysis;

using AppKit;
using Foundation;

using Einhugur.Arguments;

namespace Einhugur.Forms
{
    public class ComboBox : Control
	{
        public const int DefaultHeight = 20;
        public const int DefaultWidth = 80;

        public event EventHandler GotFocus;
        public event EventHandler LostFocus;

        //public event EventHandler<ActionEventArgs> TextChanged;
        public event EventHandler<KeyEventArgs> KeyUp;
        public event EventHandler<KeyEventArgs> KeyDown;

        public event EventHandler<ActionEventArgs> SelectionChanged;
        public event EventHandler PopupOpening;
        public event EventHandler PopupClosing;


        // a hidden private class to handle the delegate duties for various notifications
        private class ComboDelegate : NSComboBoxDelegate
        {
            readonly WeakReference<ComboBox> comboControl;

            public ComboDelegate(ComboBox comboControl)
            {
                this.comboControl = new WeakReference<ComboBox>(comboControl);
            }

            [Export("comboBoxSelectionDidChange:")]
            public override void SelectionChanged(NSNotification notification)
            {
                if (comboControl.TryGetTarget(out ComboBox target))
                {
                    target.OnSelectionChanged(true);
                }
            }

            [Export("comboBoxWillPopUp:")]
            public override void WillPopUp(NSNotification notification)
            {
                if (comboControl.TryGetTarget(out ComboBox target))
                {
                    target.OnPopupOpening();
                }
            }

            [Export("comboBoxWillDismiss:")]
            public override void WillDismiss(NSNotification notification)
            {
                if (comboControl.TryGetTarget(out ComboBox target))
                {
                    target.OnPopupClosing();
                }
            }
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        [SuppressMessage("ReSharper", "UnusedParameter.Local")]
        private sealed class NSComboBoxEx : NSComboBox
        {
            readonly WeakReference<ComboBox> owner;
            public nint lastSelectedIndex;
            private NSObject watcher;
            private bool hasFocus;

            public NSComboBoxEx(CoreGraphics.CGRect frameRect, ComboBox owner)
                : base(frameRect)
            {
                Target = this;
                lastSelectedIndex = 0;

                this.owner = new WeakReference<ComboBox>(owner);
            }


            public void Deactivate()
            {
                // Pull down the key down filter.
                if (hasFocus && watcher != null)
                {
                    NSEvent.RemoveMonitor(watcher);
                    watcher = null;
                }
            }

            public void Activate()
            {
                // Restore the Key down filter if needed
                if (hasFocus && watcher == null)
                {
                    if (owner.TryGetTarget(out var combo))
                    {
                        if (combo.KeyDown != null)
                        {
                            // put the key filter back in place
                            watcher = NSEvent.AddLocalMonitorForEventsMatchingMask(NSEventMask.KeyDown, KeyFilter);
                        }
                    }
                }
            }

            public override void DidEndEditing(NSNotification notification)
            {
                if (watcher != null)
                {
                    NSEvent.RemoveMonitor(watcher);
                    watcher = null;
                }

                base.DidEndEditing(notification);

                hasFocus = false;

                if (owner.TryGetTarget(out var combo))
                {
                    combo.OnLostFocus();
                }
            }

            public override bool ResignFirstResponder()
            {
                hasFocus = false;

                if (owner.TryGetTarget(out ComboBox target))
                {
                    target.OnLostFocus();
                }

                return base.ResignFirstResponder();
            }

            public override bool BecomeFirstResponder()
            {
                hasFocus = true;

                if (owner.TryGetTarget(out var combo))
                {
                    if (combo.KeyDown != null)
                    {
                        // put the key filter back in place
                        watcher = NSEvent.AddLocalMonitorForEventsMatchingMask(NSEventMask.KeyDown, KeyFilter);
                    }

                    combo.OnGotFocus();
                }

                return base.BecomeFirstResponder();
            }

            private NSEvent KeyFilter(NSEvent theEvent)
            {
                if (owner.TryGetTarget(out ComboBox target))
                {
                    if (target.OnKeyDown(theEvent.KeyCode, theEvent.Characters, theEvent.CharactersIgnoringModifiers, (KeyModifierFlags)theEvent.ModifierFlags))
                    {
                        return null;
                    }
                }

                return theEvent;
            }

            public override void KeyUp(NSEvent theEvent)
            {
                if (owner.TryGetTarget(out ComboBox combo))
                {
                    if (combo.OnKeyUp(theEvent.KeyCode, theEvent.Characters, theEvent.CharactersIgnoringModifiers, (KeyModifierFlags)theEvent.ModifierFlags))
                    {
                        return;
                    }
                }

                base.KeyUp(theEvent);
            }
        }

        public ComboBox(float x, float y) : this(x, y, DefaultWidth, DefaultHeight)
        { }

        public ComboBox(float x, float y, float width, float height)
        {
			Handle = new NSComboBoxEx(new CoreGraphics.CGRect(x, y, width, height), this);

            // make sure we allocate & attach the delegate !
            ((NSComboBox)Handle).Delegate = new ComboDelegate(this);
        }

        // the ComboDelegate will find & call these 
        protected virtual void OnSelectionChanged(bool invokedByUser)
        {
            SelectionChanged?.Invoke(this, new ActionEventArgs(invokedByUser));
        }

        protected virtual void OnPopupClosing()
        {
            PopupClosing?.Invoke(this, ActionEventArgs.Empty);
        }

        protected virtual void OnPopupOpening()
        {
            PopupOpening?.Invoke(this, ActionEventArgs.Empty);
        }

        public int SelectedIndex
        {
            get => (int)((NSComboBoxEx)Handle).SelectedIndex;
            set
            {
                ((NSComboBoxEx)Handle).SelectItem(value);
                ((NSComboBoxEx)Handle).lastSelectedIndex = value;

                OnSelectionChanged(false);
            }
        }

        // internal private methods that the subclass will call to raise events
        protected virtual bool OnKeyDown(ushort keyCode, string characters, string charactersIgnoringModifiers, KeyModifierFlags modifierFlags)
        {
            // We do not do pre-check here on the event since the Key down filter would not have been running if
            // no event was wired.

            var args = new KeyEventArgs(keyCode, characters, charactersIgnoringModifiers, modifierFlags);

            KeyDown?.Invoke(this, args);

            return args.Handled;
        }

        // internal private methods that the subclass will call to raise events
        protected virtual bool OnKeyUp(ushort keyCode, string characters, string charactersIgnoringModifiers, KeyModifierFlags modifierFlags)
        {
            // We do not do pre-check here on the event since the Key down filter would not have been running if
            // no event was wired.

            var args = new KeyEventArgs(keyCode, characters, charactersIgnoringModifiers, modifierFlags);

            KeyUp?.Invoke(this, args);

            return args.Handled;
        }

        // public properties 
        public bool Enabled
        {
            get => ((NSComboBox)Handle).Enabled; // a nice simple computed property that returns the Enabled property of the class we wrap
            set => ((NSComboBox)Handle).Enabled = value;
        }

        public string SelectedTitle => ((NSComboBoxEx)Handle).SelectedValue.ToString(); // a "getter only" computed property

        public string ToolTip
        {
            get => ((NSComboBoxEx)Handle).ToolTip;
            set => ((NSComboBoxEx)Handle).ToolTip = value;
        }

        public int Count => (int)(((NSComboBoxEx)Handle).Count);

        public string Placeholder
        {
            get => ((NSComboBoxEx)Handle).PlaceholderString;
            set => ((NSComboBoxEx)Handle).PlaceholderString = value;
        }

        // public methods
        public void AddItem(string title)
        {
            NSString nsTitle = new NSString(title);
            
            ((NSComboBox)Handle).Add(nsTitle);
        }

        //public void AddItems(string[] titles)
        //{
        //    ((NSComboBoxEx)Handle).Add(titles);
        //}

        public void RemoveItem(int index)
        {
            ((NSComboBoxEx)Handle).RemoveAt(index);
        }

        public void RemoveAllItems()
        {
            ((NSComboBoxEx)Handle).RemoveAll();
        }


        public string[] Items
        {
            get
            {
                string[] items = new string[((NSComboBoxEx)Handle).Count];

                for ( int i = 0; i < ((NSComboBoxEx)Handle).Count; i++ )
                {
                    NSObject item = ((NSComboBoxEx)Handle).GetItemObject(i);

                    items.SetValue(item.ToString(), i);
                }

                return items;
            }
            set
            {
                //bool selectionChanged = (int)((NSComboBoxEx)Handle).ItemCount > 0 && ((NSComboBoxEx)Handle).IndexOfSelectedItem >= 0;

                //if (((NSComboBoxEx)Handle).ItemCount > 0)
                //{
                //    ((NSComboBoxEx)Handle).RemoveAllItems();
                //}
                //((NSComboBoxEx)Handle).AddItems(value);

                //if (selectionChanged)
                //{
                //    OnSelectionChanged(false);
                //}
            }
        }

        // these basically act like "raise event"
        // the NSComboBoxEx finds & calls these which end up invoking the event handler IF it has been set
        protected virtual void OnGotFocus()
        {
            GotFocus?.Invoke(this, EventArgs.Empty);
        }

        protected virtual void OnLostFocus()
        {
            LostFocus?.Invoke(this, EventArgs.Empty);
        }

        public override void Dispose()
        {
            GotFocus = null;
            LostFocus = null;
            SelectionChanged = null;

            base.Dispose();
        }

    }
}

