﻿using System;
using System.Diagnostics.CodeAnalysis;
using AppKit;
using CoreGraphics;
using Einhugur.Arguments;
using Einhugur.Dialogs;
using Einhugur.Drawing;
using Foundation;

namespace Einhugur.Forms
{
    [SuppressMessage("ReSharper", "MemberCanBeProtected.Global")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public partial class TextField : Control
    {

        public const int DefaultHeight = 22;
        public const int DefaultWidth = 80;

        private int maximumLength;
        
        public event EventHandler<ActionEventArgs> TextChanged;
        public event EventHandler<KeyEventArgs> KeyUp;

        public event EventHandler<KeyEventArgs> KeyDown;
        public event EventHandler GotFocus;
        public event EventHandler LostFocus;

        private class LengthFormatter : NSNumberFormatter
        {
            private readonly int maxLength;

            public LengthFormatter(int maxLength)
            {
                this.maxLength = maxLength;
            }
            
            public override bool IsPartialStringValid(string partialString, out string newString, out NSString error)
            {
                error = new NSString("");
                if (partialString.Length <= maxLength)
                {
                    newString = partialString;
                    return true;
                }

                newString = partialString.Substring(0, maxLength);
                return false;
            }
        }

        internal TextField()
        {
            
        }

        public TextField(float x, float y) : this(x, y, DefaultWidth, DefaultHeight)
        { }

        public TextField(float x, float y, float width, float height)
        {
            Handle = new NSTextFieldEx(new CoreGraphics.CGRect(x, y, width, height), this)
            {
                BezelStyle = NSTextFieldBezelStyle.Square
            };

            ((NSTextField)Handle).Changed += OnTextChanged;


        }

        internal override void Deactivate()
        {
            base.Deactivate();
            
            ((NSTextFieldEx)Handle).Deactivate();
        }
        
        internal override void Activate()
        {
            base.Activate();
            
            ((NSTextFieldEx)Handle).Activate();
        }

        public int SelectionStart => (int)((NSTextField) Handle).Cell.AccessibilitySelectedTextRange.Location;
        public int SelectionLength => (int)((NSTextField) Handle).Cell.AccessibilitySelectedTextRange.Length;
        
        public string SelectedText
        {
            get => ((NSTextField) Handle).Cell.AccessibilitySelectedText;
            set
            {
                if (value != ((NSTextField)Handle).Cell.AccessibilitySelectedText)
                {
                    ((NSTextField)Handle).Cell.AccessibilitySelectedText = value;
                    OnTextChanged(false);
                }
            }
        }

        public void Select(int start, int length)
        {
            ((NSTextField) Handle).Cell.AccessibilitySelectedTextRange = new NSRange(start, length);
        }

        public bool Editable
        {
            get => ((NSTextField) Handle).Editable;
            set
            {
                ((NSTextField) Handle).Editable = value;
                ((NSTextField)Handle).Selectable = true;
            }
        }

        public bool Selectable
        {
            get => ((NSTextField)Handle).Selectable;
            set
            {
                // the interesting thing here is IF the fiel dhas focus when you switch
                // selectable it will retain whatever setting it has until it loses focus
                // and regains it
                // at that point it will be whatever the new setting was
                ((NSTextField)Handle).Selectable = value;
            }
        }

        public int MaximumLength
        {
            get => maximumLength;
            set
            {
                if (value != maximumLength)
                {
                    var nsObject = ((NSTextField) Handle).Formatter;
                    if (nsObject != null) nsObject.Dispose();

                    ((NSTextField) Handle).Formatter = value == 0 ? null : new LengthFormatter(value);

                    maximumLength = value;
                }
            }
        }
        
        public Color TextColor
        {
            get => new Color(((NSTextField)Handle).TextColor.CGColor);
            set => ((NSTextField)Handle).TextColor = value.AsNSColor;
        }
        
        public Color BackgroundColor
        {
            get => new Color(((NSTextField)Handle).BackgroundColor.CGColor);
            set => ((NSTextField)Handle).BackgroundColor = value.AsNSColor;
        }
        
        public bool HasBackgroundColor
        {
            get => ((NSTextField)Handle).DrawsBackground;
            set => ((NSTextField)Handle).DrawsBackground = value;
        }
        
        public bool ShowFocusRing
        {
            get => ((NSTextField)Handle).FocusRingType != NSFocusRingType.None;
            set => ((NSTextField)Handle).FocusRingType = value ? NSFocusRingType.Default : NSFocusRingType.None;
        }

        public bool RoundedFrame
        {
            get => ((NSTextField) Handle).BezelStyle == NSTextFieldBezelStyle.Rounded;
            set => ((NSTextField) Handle).BezelStyle = value ? NSTextFieldBezelStyle.Rounded : NSTextFieldBezelStyle.Square;
        }
        
        public bool HasFrame
        {
            get => ((NSTextField) Handle).Bezeled;
            set => ((NSTextField) Handle).Bezeled = value;
        }

        protected internal virtual void OnGotFocus()
        {
            GotFocus?.Invoke(this, EventArgs.Empty);
        }

        protected internal virtual void OnLostFocus()
        {
            LostFocus?.Invoke(this, EventArgs.Empty);
        }

        public virtual void SelectAll()
        {
            ((NSTextField)Handle).SelectText(this.Handle);
        }

        internal void OnTextChanged(object sender, EventArgs e)
        {
            OnTextChanged(true);
        }

        protected virtual bool OnKeyDown(ushort keyCode, string characters, string charactersIgnoringModifiers, KeyModifierFlags modifierFlags)
        {
            // We do not do pre-check here on the event since the Key down filter would not have been running if
            // no event was wired.

            var args = new KeyEventArgs(keyCode, characters, charactersIgnoringModifiers, modifierFlags);

            KeyDown?.Invoke(this, args);

            return args.Handled;
        }
        
        protected virtual bool OnKeyUp(ushort keyCode, string characters, string charactersIgnoringModifiers, KeyModifierFlags modifierFlags)
        {
            if (KeyUp == null)
            {
                return false;
            }

            var args = new KeyEventArgs(keyCode, characters, charactersIgnoringModifiers, modifierFlags);

            KeyUp?.Invoke(this, args);

            return args.Handled;
        }

        protected virtual void OnTextChanged(bool invokedByUser)
        {
            TextChanged?.Invoke(this, new ActionEventArgs(invokedByUser));
        }

        public string Text
        {
            get => ((NSTextField)Handle).StringValue;
            set
            {
                if (value != ((NSTextField)Handle).StringValue)
                {
                    ((NSTextField)Handle).StringValue = value;
                    OnTextChanged(false);
                }
            }
        }
        
        public string Placeholder
        {
            get => ((NSTextField)Handle).PlaceholderString;
            set => ((NSTextField)Handle).PlaceholderString = value;
        }
        
        public string Tooltip
        {
            get => ((NSTextField)Handle).ToolTip;
            set => ((NSTextField)Handle).ToolTip = value;
        }

        public bool Enabled
        {
            get => ((NSTextField)Handle).Enabled;
            set => ((NSTextField)Handle).Enabled = value;
        }
        
        public override void DrawInto(GraphicsContext g, float x, float y)
        {
            NSTextField tf = new NSTextField(new CGRect(0, 0, this.Width, this.Height));
            tf.WantsLayer = true;
            tf.PlaceholderString = this.Placeholder;
            tf.StringValue = this.Text;
            tf.Bezeled = ((NSTextField) this.Handle).Bezeled;
            tf.BezelStyle = ((NSTextField) this.Handle).BezelStyle;
            tf.DrawsBackground = ((NSTextField) this.Handle).DrawsBackground;
            tf.BackgroundColor = ((NSTextField) this.Handle).BackgroundColor;
            tf.TextColor = ((NSTextField) this.Handle).TextColor;
            
            tf.Enabled = this.Enabled;
            
            g.Handle.SaveGraphicsState();
            var tr = new NSAffineTransform();
            tr.Translate(x, y);
            tr.Concat();
            
            //btn.DisplayRect(new CGRect(0, 0, this.Width, this.Height) );
            tf.DisplayRectIgnoringOpacity(new CGRect(0, 0, this.Width, this.Height), g.Handle);

            g.Handle.RestoreGraphicsState();
        }

        public override void Dispose()
        {
            ((NSTextField)Handle).Changed -= OnTextChanged;
            TextChanged = null;
            GotFocus = null;
            LostFocus = null;
            KeyUp = null;
            KeyDown = null;

            base.Dispose();
        }
    }
}

