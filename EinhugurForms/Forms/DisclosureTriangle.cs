﻿using System;
using AppKit;

namespace Einhugur.Forms
{
    public enum FacingDirections : long
    {
        // right to left faces the point of the triangle LEFT when the STATE is OFF
        // left to right faces RIGHT when the STATE is OFF
        // both face the point DOWN when the state is ON

        Left = NSUserInterfaceLayoutDirection.RightToLeft,
        Right = NSUserInterfaceLayoutDirection.LeftToRight
    }

    public class DisclosureTriangle : Button
    {

        public new const int DefaultHeight = 18;
        public new const int DefaultWidth = 18;

        public DisclosureTriangle(float x, float y) : this(x, y, DefaultWidth, DefaultHeight)
        { }

        public DisclosureTriangle(float x, float y, float width, float height) : base(x, y, width, height)
        {
            ((NSButton)Handle).BezelStyle = NSBezelStyle.Disclosure;
            ((NSButton)Handle).Bordered = true;
            ((NSButton)Handle).Title = "";
            ((NSButton)Handle).State = NSCellStateValue.Off; // ON = OPEN or down facing OFF = closed or right facing
            ((NSButton)Handle).AllowsMixedState = false;
            ((NSButton)Handle).IsSpringLoaded = false;
            ((NSButton)Handle).ImagePosition = NSCellImagePosition.ImageOnly;
            ((NSButton)Handle).Enabled = true;
            ((NSButton)Handle).SetButtonType(NSButtonType.PushOnPushOff);
            ((NSButton)Handle).UserInterfaceLayoutDirection = (NSUserInterfaceLayoutDirection)FacingDirections.Left;
        }

        public FacingDirections Facing
        {
            get => (FacingDirections)(((NSButton)Handle).UserInterfaceLayoutDirection);
            set => ((NSButton)Handle).UserInterfaceLayoutDirection = (NSUserInterfaceLayoutDirection)value;
        }

        public bool State
        {
            get => (NSCellStateValue.On == ((NSButton)Handle).State)  ? true : false;
            set => ((NSButton)Handle).State = value ? NSCellStateValue.On : NSCellStateValue.Off;
        }

        // this_should_hide the caption of a button in the DisclosureTriangle
        // they really dont have one but if you manage to set one
        // it tries to draw it and things look like crap !
        public new string Caption
        {
            get => base.Caption;
            set { }
        }

    }
}

