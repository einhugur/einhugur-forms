using AppKit;
using Foundation;

namespace Einhugur.Forms
{
    public class System
    {
        public static string EnvironmentVariable(string name)
        {
            var nsName = new NSString(name);
            return NSProcessInfo.ProcessInfo.Environment.ContainsKey(nsName) ? (NSString)NSProcessInfo.ProcessInfo.Environment[nsName] : null;
        }
    }
}