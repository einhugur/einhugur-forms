﻿using System;
using System.Diagnostics.CodeAnalysis;

using AppKit;
using Foundation;
using ObjCRuntime;

using Einhugur.Arguments;

namespace Einhugur.Forms
{
    public enum DatePickerStyle
    {
        TextFieldAndStepper = 0,
        ClockAndCalendar = 1,
        TextField = 2
    }
       

    public class DateTimePicker : Control
    {
        public event EventHandler GotFocus;
        public event EventHandler LostFocus;
        public event EventHandler Changed;

        public DateTimePicker(float x, float y, float width, float height)
        {
            Handle = new NSDatePickerEx(new CoreGraphics.CGRect(x, y, width, height), this);
            ((NSDatePicker)Handle).DateValue = NSDate.Now;
            // we want to respond to date/time changes - the Control's Action 
            ((NSControl)Handle).Action = new ObjCRuntime.Selector("datePickerAction:");

        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        [SuppressMessage("ReSharper", "UnusedParameter.Local")]
        private sealed class NSDatePickerEx : NSDatePicker
        {
            readonly WeakReference<DateTimePicker> owner;

            public NSDatePickerEx(CoreGraphics.CGRect frameRect, DateTimePicker owner)
                : base(frameRect)
            {
                Target = this;

                this.owner = new WeakReference<DateTimePicker>(owner);

            }

            public override bool ResignFirstResponder()
            {

                if (owner.TryGetTarget(out DateTimePicker target))
                {
                    target.OnLostFocus();
                }

                return base.ResignFirstResponder();
            }

            public override bool BecomeFirstResponder()
            {

                if (owner.TryGetTarget(out DateTimePicker target))
                {
                    target.OnGotFocus();
                }

                return base.BecomeFirstResponder();
            }
                        
            /// <summary>
            /// Respond to date/time changes of the NSDatePicker control.  This is the method
            /// that is called as the user interacts with the control.
            /// </summary>
            [Export("datePickerAction:")]
            private void datePickerAction(NSDatePicker picker)
            {
                if (owner.TryGetTarget(out DateTimePicker target))
                {
                    target.OnChange();
                }
            }
        }

        public DatePickerStyle Style
        {
            get => (DatePickerStyle)((NSDatePicker)Handle).DatePickerStyle;
            set => ((NSDatePicker)Handle).DatePickerStyle = (NSDatePickerStyle)value;
        }

        public DateTime SelectedDate
        {
            get
            {
                NSDate workingDate = (NSDate)(((NSDatePicker)Handle).DateValue);
                // date picker is documented as using current calendar & TZ
                NSCalendar workingCalendar = NSCalendar.CurrentCalendar;
                // NSTimeZone workingTimeZone = NSTimeZone.DefaultTimeZone;

                // set up all the components we want the thing to compute from the date
                NSCalendarUnit flags = NSCalendarUnit.Year | NSCalendarUnit.Month | NSCalendarUnit.Day | NSCalendarUnit.Hour | NSCalendarUnit.Minute | NSCalendarUnit.Second;

                // compute all the bits from the date
                NSDateComponents dateComponents = workingCalendar.Components(flags , workingDate);

                // get all the bits
                int year = (int)(dateComponents.Year);
                int month = (int)(dateComponents.Month);
                int day = (int)(dateComponents.Day);
                int hour = (int)(dateComponents.Hour);
                int minute = (int)(dateComponents.Minute);
                int second = (int)(dateComponents.Second);

                return new DateTime(year, month, day, hour, minute, second, DateTimeKind.Local);
            }
        }

        // got focus
        // these basically act like "raise event"
        // the NSComboBoxEx finds & calls these which end up invoking the event handler IF it has been set
        protected virtual void OnGotFocus()
        {
            GotFocus?.Invoke(this, EventArgs.Empty);
        }
               
        // lost focus
        protected virtual void OnLostFocus()
        {
            LostFocus?.Invoke(this, EventArgs.Empty);
        }

        // changed
        protected virtual void OnChange()
        {
            Changed?.Invoke(this, EventArgs.Empty);
        }
    }
}

