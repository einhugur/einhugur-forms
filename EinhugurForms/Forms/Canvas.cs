﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;
using AppKit;
using CoreGraphics;
using Einhugur.Drawing;
using Einhugur.Arguments;
using Einhugur.Menus;
using Foundation;

namespace Einhugur.Forms
{
    public class Canvas : ContainerControl
    {
        public event EventHandler<PaintEventArgs> Paint;
        public event EventHandler GotFocus;
        public event EventHandler LostFocus;
        public event EventHandler<MouseClickEventArgs> MouseDown;
        public event EventHandler<MouseClickEventArgs> MouseUp;
        public event EventHandler MouseEntered;
        public event EventHandler MouseExited;
        public event EventHandler<MouseWheelEventArgs> MouseWheel;
        public event EventHandler<MouseEventArgs> MouseMoved;
        public event EventHandler<KeyEventArgs> KeyDown;
        public event EventHandler<KeyEventArgs> KeyUp;
        public event EventHandler<SpecialMenuEventEnablerArgs> EnableSpecialMenu;
        public event EventHandler<SpecialMenuEventArgs> SpecialMenuAction;

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        [SuppressMessage("ReSharper", "UnusedParameter.Local")]
        [SuppressMessage("ReSharper", "UnusedMember.Local")]
        private sealed class NSViewEx : NSView
        {
            readonly WeakReference<Canvas> owner;

            private bool hasFocus;
            private bool acceptsFocus;
            private bool showFocusRing;
            private bool mouseTracking;

            public NSViewEx(
                CGRect bounds,
                Canvas owner
                )
                : base(bounds)
            {
                this.FocusRingType = NSFocusRingType.Default;
                this.owner = new WeakReference<Canvas>(owner);
            }

            [Export("respondsToSelector:")]
            public override bool RespondsToSelector(ObjCRuntime.Selector sel)
            {
                var result = base.RespondsToSelector(sel);

                if (result)
                {
                    return true;
                }

                if (SpecialMenuItem.IsSpecialSelector(sel.Name))
                {
                    if (owner.TryGetTarget(out Canvas canvas))
                    {
                        return canvas.OnEnableSpecialMenu(SpecialMenuItem.SpecialCommandFromSelector(sel.Name));
                    }
                }

                return false;
            }


            [Export("cut:")]
            public void cut(NSObject sender)
            {
                if (owner.TryGetTarget(out Canvas canvas))
                {
                    canvas.OnSpecialMenuItem(SpecialMenuItem.SpecialCommand.Cut);
                }
            }

            [Export("copy:")]
            public void copy(NSObject sender)
            {
                if (owner.TryGetTarget(out Canvas canvas))
                {
                    canvas.OnSpecialMenuItem(SpecialMenuItem.SpecialCommand.Copy);
                }
            }

            [Export("paste:")]
            public void paste(NSObject sender)
            {
                if (owner.TryGetTarget(out Canvas canvas))
                {
                    canvas.OnSpecialMenuItem(SpecialMenuItem.SpecialCommand.Paste);
                }
            }

            [Export("selectAll:")]
            public void selectAll(NSObject sender)
            {
                if (owner.TryGetTarget(out Canvas canvas))
                {
                    canvas.OnSpecialMenuItem(SpecialMenuItem.SpecialCommand.SelectAll);
                }
            }

            [Export("delete:")]
            public void delete(NSObject sender)
            {
                if (owner.TryGetTarget(out Canvas canvas))
                {
                    canvas.OnSpecialMenuItem(SpecialMenuItem.SpecialCommand.Delete);
                }
            }

            public override bool IsFlipped => true;

            [DllImport("/System/Library/Frameworks/AppKit.framework/AppKit", EntryPoint = "NSSetFocusRingStyle")]
            private static extern void SetFocusRingStyle(NSFocusRingPlacement placement);

            [DllImport("/System/Library/Frameworks/AppKit.framework/AppKit", EntryPoint = "NSRectFill")]
            private static extern void RectFill(CGRect rect);

            public bool ShowFocusRing
            {
                get => showFocusRing;
                set
                {
                    if (value != showFocusRing)
                    {
                        showFocusRing = value;

                        if (hasFocus)
                        {
                            NeedsDisplay = true;
                        }
                    }
                }
            }

            public bool AcceptsFocus
            {
                get => acceptsFocus;
                set => acceptsFocus = value;
            }

            public bool MouseTracking
            {
                get => mouseTracking;
                set => mouseTracking = value;
            }

            public bool HasFocus => hasFocus;

            public override void ScrollWheel(NSEvent theEvent)
            {
                if (theEvent.DeltaY != 0)
                {
                    if (owner.TryGetTarget(out var canvas))
                    {
                        var local = this.ConvertPointFromView(theEvent.LocationInWindow, null);
                        
                        canvas.OnMouseWheel(local.X,local.Y, theEvent.ScrollingDeltaX, theEvent.ScrollingDeltaY, theEvent.HasPreciseScrollingDeltas);
                    }
                    
                }

                base.ScrollWheel(theEvent);
            }

            public override void KeyUp(NSEvent theEvent)
            {
                if (owner.TryGetTarget(out var canvas))
                {
                    if (canvas.OnKeyUp(theEvent.KeyCode, theEvent.Characters, theEvent.CharactersIgnoringModifiers, (KeyModifierFlags)theEvent.ModifierFlags))
                    {
                        return;
                    }
                }

                base.KeyUp(theEvent);
            }

            public override void KeyDown(NSEvent theEvent)
            {
                if (owner.TryGetTarget(out var canvas))
                {
                    if (canvas.OnKeyDown(theEvent.KeyCode, theEvent.Characters, theEvent.CharactersIgnoringModifiers, (KeyModifierFlags)theEvent.ModifierFlags))
                    {
                        return;
                    }
                }

                base.KeyDown(theEvent);
            }

            public override void UpdateTrackingAreas()
            {
                base.UpdateTrackingAreas();

                if (mouseTracking)
                {
                    foreach (var item in TrackingAreas())
                    {
                        RemoveTrackingArea(item);
                    }

                    var options = NSTrackingAreaOptions.MouseEnteredAndExited | NSTrackingAreaOptions.ActiveAlways;

                    var trackingArea = new NSTrackingArea(this.Bounds, options, this, null);

                    AddTrackingArea(trackingArea);
                }
            }


            public override void MouseEntered(NSEvent theEvent)
            {
                if (owner.TryGetTarget(out Canvas canvas))
                {
                    canvas.OnMouseEntered();
                }
            }

            public override void MouseExited(NSEvent theEvent)
            {
                if (owner.TryGetTarget(out Canvas canvas))
                {
                    canvas.OnMouseExited();
                }
            }

            public override void MouseMoved(NSEvent theEvent)
            {
                if (owner.TryGetTarget(out Canvas canvas))
                {
                    var local = this.ConvertPointFromView(theEvent.LocationInWindow, null);

                    canvas.OnMouseMoved(local.X, local.Y);
                }
            }

            public override void MouseUp(NSEvent theEvent)
            {
                if (owner.TryGetTarget(out Canvas canvas))
                {
                    var local = this.ConvertPointFromView(theEvent.LocationInWindow, null);

                    canvas.OnMouseUp(local.X, local.Y, MouseButton.Left);
                }
            }

            public override void RightMouseUp(NSEvent theEvent)
            {
                if (owner.TryGetTarget(out Canvas canvas))
                {
                    var local = this.ConvertPointFromView(theEvent.LocationInWindow, null);

                    canvas.OnMouseUp(local.X, local.Y, MouseButton.Right);
                }
            }

            public override void MouseDown(NSEvent theEvent)
            {
                if (owner.TryGetTarget(out Canvas canvas))
                {
                    var local = this.ConvertPointFromView(theEvent.LocationInWindow, null);

                    canvas.OnMouseDown(local.X, local.Y, MouseButton.Left);
                }
            }

            public override void RightMouseDown(NSEvent theEvent)
            {
                if (owner.TryGetTarget(out Canvas canvas))
                {
                    var local = this.ConvertPointFromView(theEvent.LocationInWindow, null);

                    canvas.OnMouseDown(local.X, local.Y, MouseButton.Right);
                }
            }

            public override void DrawRect(CGRect dirtyRect)
            {
                NSGraphicsContext ctx = NSGraphicsContext.CurrentContext;

                if (ctx != null)
                {

                    ctx.SaveGraphicsState();

                    if (owner.TryGetTarget(out Canvas canvas))
                    {
                        using (var g = new GraphicsContext(ctx, Frame.Size))
                        {
                            canvas.OnPaint(g);

                            if (ctx.Handle != NSGraphicsContext.CurrentContext?.Handle)
                            {
                                NSGraphicsContext.CurrentContext = ctx;
                            }
                        }
                    }

                    ctx.RestoreGraphicsState();

                    if (acceptsFocus && hasFocus && showFocusRing)
                    {
                        SetFocusRingStyle(NSFocusRingPlacement.RingOnly);
                        RectFill(new CGRect(0, 0, Frame.Width, Frame.Height));
                    }

                }
            }

            public override bool CanBecomeKeyView => acceptsFocus;

            public override bool BecomeFirstResponder()
            {
                if (acceptsFocus)
                {
                    hasFocus = true;

                    if (showFocusRing)
                    {
                        this.NeedsDisplay = true;
                    }
                }

                if(!acceptsFocus)
                {
                    return false;
                }

                if (owner.TryGetTarget(out Canvas canvas))
                {
                    

                    canvas.OnGotFocus();
                }

                return base.BecomeFirstResponder();
            }

            public override bool ResignFirstResponder()
            {
                if (acceptsFocus || hasFocus)
                {
                    hasFocus = false;

                    if (showFocusRing)
                    {
                        this.NeedsDisplay = true;
                    }
                }

                if (owner.TryGetTarget(out Canvas canvas))
                {
                    canvas.OnLostFocus();
                }

                return base.ResignFirstResponder();
            }

            public override bool AcceptsFirstResponder()
            {
                return true;
            }
        }

        public Canvas(float x, float y, float width, float height, Func<IEnumerable<Control>> setupControls = null)
        {
            Handle = new NSViewEx(
                new CGRect(x, y, width, height),
                this);

            if (setupControls != null)
            {
                SetupControls(setupControls);
            }
        }

        

        public bool ShowFocusRing
        {
            get => ((NSViewEx)Handle).ShowFocusRing;
            set => ((NSViewEx)Handle).ShowFocusRing = value;
        }

        public bool AcceptsFocus
        {
            get => ((NSViewEx)Handle).AcceptsFocus; 
            set => ((NSViewEx)Handle).AcceptsFocus = value;
        }

        public bool MouseTracking
        {
            get => ((NSViewEx)Handle).MouseTracking;
            set => ((NSViewEx)Handle).MouseTracking = value;
        }
        
        public string ToolTip
        {
            get => ((NSViewEx) Handle).ToolTip;
            set => ((NSViewEx) Handle).ToolTip = value;
        }

        public bool HasFocus => ((NSViewEx)Handle).HasFocus;

        protected virtual void OnMouseWheel(nfloat x, nfloat y, nfloat scrollingDeltaX, nfloat scrollingDeltaY, bool hasPreciseScrollingDeltas)
        {
            MouseWheel?.Invoke(this, new MouseWheelEventArgs(x, y, scrollingDeltaX, scrollingDeltaY, hasPreciseScrollingDeltas));
        }

        protected virtual bool OnKeyDown(ushort keyCode, string characters, string charactersIgnoringModifiers, KeyModifierFlags modifierFlags)
        {
            if (KeyDown == null)
            {
                return false;
            }

            var args = new KeyEventArgs(keyCode, characters, charactersIgnoringModifiers, modifierFlags);

            KeyDown?.Invoke(this, args);

            return args.Handled;
        }


        protected virtual bool OnKeyUp(ushort keyCode, string characters, string charactersIgnoringModifiers, KeyModifierFlags modifierFlags)
        {
            if (KeyUp == null)
            {
                return false;
            }

            var args = new KeyEventArgs(keyCode, characters, charactersIgnoringModifiers, modifierFlags);

            KeyUp?.Invoke(this, args);

            return args.Handled;
        }


        protected virtual void OnMouseDown(nfloat x, nfloat y, MouseButton button)
        {
            MouseDown?.Invoke(this, new MouseClickEventArgs(x, y, button));
        }

        protected virtual void OnMouseUp(nfloat x, nfloat y, MouseButton button)
        {
            MouseUp?.Invoke(this, new MouseClickEventArgs(x, y, button));
        }

        protected virtual void OnMouseMoved(nfloat x, nfloat y)
        {
            MouseMoved?.Invoke(this, new MouseEventArgs(x, y));
        }

        protected virtual void OnMouseEntered()
        {
            MouseEntered?.Invoke(this, EventArgs.Empty);
        }

        protected virtual void OnMouseExited()
        {
            MouseExited?.Invoke(this, EventArgs.Empty);
        }

        protected virtual void OnGotFocus()
        {
            GotFocus?.Invoke(this, EventArgs.Empty);
        }

        protected virtual void OnLostFocus()
        {
            LostFocus?.Invoke(this, EventArgs.Empty);
        }

        protected virtual void OnPaint(GraphicsContext g)
        {
            Paint?.Invoke(this, new PaintEventArgs(g));
        }

        protected virtual void OnSpecialMenuItem(SpecialMenuItem.SpecialCommand command)
        {
            SpecialMenuAction?.Invoke(this, new SpecialMenuEventArgs(command));
        }

        protected virtual bool OnEnableSpecialMenu(SpecialMenuItem.SpecialCommand command)
        {
            // Quick switch out for speed reasons.
            if (EnableSpecialMenu == null)
            {
                return false;
            }

            var args = new SpecialMenuEventEnablerArgs(command);

            EnableSpecialMenu?.Invoke(this, args);

            return args.Enabled;
        }

        public override void Dispose()
        {
            Paint = null;
            GotFocus = null;
            LostFocus = null;
            MouseDown = null;
            MouseUp = null;
            MouseEntered = null;
            MouseExited = null;
            MouseWheel = null;
            MouseMoved = null;
            KeyUp = null;
            KeyDown = null;
            EnableSpecialMenu = null;
            SpecialMenuAction = null;

            base.Dispose();
        }
    }
}

