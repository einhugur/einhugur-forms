﻿using System;
using System.Collections.Generic;
using AppKit;
using Foundation;
using System.Linq;

namespace Einhugur.Forms
{
	public class ComboButton : Control
	{
        public enum ComboButtonStyle
        {
            Split = 0,
            Unified = 1,
        }


        public event EventHandler Pressed;
        public event EventHandler GotFocus;
        public event EventHandler LostFocus;

        private NSMenuItem[] storedItemsInDisabledState;

        internal class NSComboButtonEx : NSComboButton
        {
            private readonly Action gotFocusHandler;
            private readonly Action lostFocusHandler;
            private readonly Action pressedHandler;

            public NSComboButtonEx(CoreGraphics.CGRect frameRect, Action gotFocusHandler, Action lostFocusHandler, Action pressedHandler)
                : base(frameRect)
            {
                this.gotFocusHandler = gotFocusHandler;
                this.lostFocusHandler = lostFocusHandler;
                this.pressedHandler = pressedHandler;
            }

            public override bool BecomeFirstResponder()
            {
                gotFocusHandler();

                return base.BecomeFirstResponder();
            }

            public override bool ResignFirstResponder()
            {
                lostFocusHandler();

                return base.ResignFirstResponder();
            }

         
            [Export("action:")]
            private void actionHandler(NSComboButton button)
            {
                pressedHandler();
            }

       
        }

        

        public ComboButton(float x, float y, float width, float height)
        {
            Handle = new NSComboButtonEx(new CoreGraphics.CGRect(x, y, width, height), OnGotFocus, OnLostFocus, OnPressed);

            ((NSComboButton)Handle).Style = NSComboButtonStyle.Split;

            ((NSComboButton)Handle).Target = Handle;
            ((NSComboButton)Handle).Action = new ObjCRuntime.Selector("action:");
        }

        public void AddMenuItem(Menus.MenuItem item)
        {
            if(item != null)
            {
                ((NSComboButton)Handle).Menu.AddItem(item);
            }
        }

        public void RemoveAllMenuItems()
        {
            ((NSComboButton)Handle).Menu.RemoveAllItems();
        }

        private void ToggleMenuButtonEnabled()
        {
            // We do this to force the control to re-evaluate it self else the button will never change state
            if(((NSComboButton)Handle).Style == NSComboButtonStyle.Unified)
            {
                ((NSComboButton)Handle).Style = NSComboButtonStyle.Split;
                ((NSComboButton)Handle).Style = NSComboButtonStyle.Unified;
            }
            else
            {
                ((NSComboButton)Handle).Style = NSComboButtonStyle.Unified;
                ((NSComboButton)Handle).Style = NSComboButtonStyle.Split;
            }
        }

        public IEnumerable<Menus.MenuItem> MenuItems
        {
            get => ((NSComboButton)Handle).Menu.Items.Select(x => (Menus.MenuItem)x);
            set
            {
                if (value == null)
                {
                    ((NSComboButton)Handle).Menu.RemoveAllItems();
                }
                else
                {
                    ((NSComboButton)Handle).Menu.Items = value.Select(x => (NSMenuItem)x).ToArray();
                }

                ToggleMenuButtonEnabled();
            }
        }

        protected virtual void OnPressed()
        {
            Pressed?.Invoke(this, EventArgs.Empty);
        }

        protected virtual void OnGotFocus()
        {
            GotFocus?.Invoke(this, EventArgs.Empty);
        }

        protected virtual void OnLostFocus()
        {
            LostFocus?.Invoke(this, EventArgs.Empty);
        }

        public string Caption
        {
            get => ((NSComboButton)Handle).Title;
            set => ((NSComboButton)Handle).Title = value;
        }

        public string ToolTip
        {
            get => ((NSComboButton)Handle).ToolTip;
            set => ((NSComboButton)Handle).ToolTip = value;
        }

        public bool Enabled
        {
            get => ((NSComboButton)Handle).Enabled;
            set => ((NSComboButton)Handle).Enabled = value;
        }

        public bool MenuEnabled
        {
            get => storedItemsInDisabledState == null;
            set
            {
                if(value && !MenuEnabled)
                {
                    ((NSComboButton)Handle).Menu.Items = storedItemsInDisabledState;
                    storedItemsInDisabledState = null;
                    ToggleMenuButtonEnabled();
                }
                else if(!value && MenuEnabled)
                {
                    storedItemsInDisabledState = ((NSComboButton)Handle).Menu.Items;
                    ((NSComboButton)Handle).Menu.RemoveAllItems();
                    ToggleMenuButtonEnabled(); 
                }
            }
        }

        public bool AutoEnableMenuItems
        {
            get => ((NSComboButton)Handle).Menu.AutoEnablesItems;
            set => ((NSComboButton)Handle).Menu.AutoEnablesItems = value;
        }

        public ComboButtonStyle Style
        {
            get => ((NSComboButton)Handle).Style == NSComboButtonStyle.Split ? ComboButtonStyle.Split : ComboButtonStyle.Unified;
            set => ((NSComboButton)Handle).Style = (value == ComboButtonStyle.Split) ? NSComboButtonStyle.Split : NSComboButtonStyle.Unified;
        }

        public override void Dispose()
        {
            Pressed = null;
            GotFocus = null;
            LostFocus = null;

            base.Dispose();
        }

    }
}

