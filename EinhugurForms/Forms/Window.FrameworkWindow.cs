using System;
using System.Collections.Generic;
using AppKit;
using CoreGraphics;
using Einhugur.Arguments;
using Einhugur.Menus;

namespace Einhugur.Forms
{
    public abstract partial class Window
    {
        internal class FrameworkWindow : NSWindow
        {
            private readonly WeakReference<Window> owner;
            private readonly Func<NSMenuItem, bool> menuValidator;
            private readonly Action disposing;
            private readonly Action resizing;
            private Dictionary<MenuItem, EventHandler<MenuEventArgs>> menuHandlers;
            internal List<Control> controls = new List<Control>();
            internal WindowPlacement InitialPlacement;

            public FrameworkWindow(
                CGRect bounds,
                NSWindowStyle style,
                WindowPlacement initialPlacement,
                Dictionary<MenuItem, EventHandler<MenuEventArgs>> menuHandlers,
                Func<NSMenuItem, bool> menuValidator,
                Action disposing,
                Action resizing,
                Window owner)
                : base(bounds, style, NSBackingStore.Buffered, false)
            {
                this.menuHandlers = menuHandlers;
                this.menuValidator = menuValidator;
                this.disposing = disposing;
                this.resizing = resizing;
                this.owner = new WeakReference<Window>(owner);
                this.InitialPlacement = initialPlacement;

                if(initialPlacement == WindowPlacement.Center)
                {
                    this.Center();
                }
            }

            internal Window Owner
            {
                get
                {
                    if(owner.TryGetTarget(out var target))
                    {
                        return target;
                    }
                    return null;
                }
            }

            internal void SetupEvents()
            {
                this.WillClose += FrameworkWindow_WillClose;
                this.DidResize += FrameworkWindow_DidResize;
            }

            internal bool MenuAction(MenuItem item)
            {
                if (menuHandlers != null && menuHandlers.ContainsKey(item))
                {
                    var args = new MenuEventArgs();
                    menuHandlers[item]?.Invoke(item, args);

                    if (args.Handled)
                    {
                        return true;
                    }
                }
                return false;
            }

            private void FrameworkWindow_DidResize(object sender, EventArgs e)
            {
                resizing();
            }

            public override void BecomeMainWindow()
            {
                foreach (var control in controls) 
                {
                    control.Activate();
                }
                
                base.BecomeMainWindow();
                
                if(owner.TryGetTarget(out var window))
                {
                    window.Activating();
                }
            }

            public override void ResignMainWindow()
            {
                foreach (var control in controls) 
                {
                    control.Deactivate();
                }

                base.ResignMainWindow();
                
                if(owner.TryGetTarget(out var window))
                {
                    window.Deactivating();
                }
            }

            /*public override bool ResignFirstResponder()
            {
                return base.ResignFirstResponder();
            }*/

            private void FrameworkWindow_WillClose(object sender, EventArgs e)
            {
                this.WillClose -= FrameworkWindow_WillClose;
                this.DidResize -= FrameworkWindow_DidResize;
                disposing(); // Calls WillClose on the owner class.
                
                foreach (var control in controls)
                {
                    control.Dispose();
                }
                
                menuHandlers = null;
                this.Dispose();
            }

            public override bool ValidateMenuItem(NSMenuItem menuItem)
            {
                if (menuValidator != null)
                {
                    return menuValidator(menuItem);
                }

                return true;
            }
        }
    }
}