﻿
using System;
using System.Collections.Generic;
using AppKit;


namespace Einhugur.Forms
{
    public enum TextAlignment
    {
        Left = 0,
        Right = 1,
        Center = 2,
        Justified = 3,
        Natural = 4
    }

    public class Label : Control
    {

        public const int DefaultHeight = 20;
        public const int DefaultWidth = 100;

        public Label(float x, float y) : this(x, y, DefaultWidth, DefaultHeight)
        { }

        public Label(float x, float y, float width, float height)
        {
            Handle = new NSTextField(new CoreGraphics.CGRect(x, y, width, height))
            {
                Bezeled = false,
                DrawsBackground = false,
                Editable = false,
                Selectable = false
            };
        }

        public string Caption
        {
            get => ((NSTextField)Handle).StringValue;
            set => ((NSTextField)Handle).StringValue = value;
        }


        public TextAlignment Alignment
        {
            get => (TextAlignment)((NSTextField)Handle).Alignment;
            set => ((NSTextField)Handle).Alignment = (NSTextAlignment)value;
        }

        public bool Enabled
        {
            get => ((NSTextField)Handle).Enabled;
            set => ((NSTextField)Handle).Enabled = value; 
        }

        public bool Selectable
        {
            get => ((NSTextField)Handle).Selectable;
            set => ((NSTextField)Handle).Selectable = value;
        }

        public string ToolTip
        {
            get => ((NSTextField)Handle).ToolTip;
            set => ((NSTextField)Handle).ToolTip = value;
        }

    }
}

