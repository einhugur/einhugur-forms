﻿using System;
using System.Collections.Generic;
using System.Linq;
using AppKit;
using CoreGraphics;
using Einhugur.Arguments;
using Einhugur.Drawing;
using Foundation;

namespace Einhugur.Forms
{
    public class ListBox : Control
    {
        public const int DefaultHeight = 200;
        public const int DefaultWidth = 300;

        readonly NSTableView tableView;


        public event EventHandler SelectionChanged;
        public event EventHandler<CellEventArgs> DoubleClickRow;

        private string[] initialColumnTitles; //TODO: Revisit this

        private class DataSourceColumn
        {
            public List<string> rows;

            public DataSourceColumn()
            {
                rows = new List<string>();
            }

            public void ResizeToRowCount(int rowCount)
            {
                while (rows.Count < rowCount)
                {
                    rows.Add("");
                }
            }

        }

        private class DataSource : NSTableViewDataSource
        {
            readonly List<DataSourceColumn> columns;
            readonly List<Picture> rowPictures;
            readonly List<dynamic> rowTags;

            public bool IsUsingImages;

            public DataSource()
            {
                IsUsingImages = false;
                columns = new List<DataSourceColumn>();
                rowPictures = new List<Picture>();
                rowTags = new List<dynamic>() ;
            }

            [Export("numberOfRowsInTableView:")]
            public override nint GetRowCount(NSTableView tableView)
            {
                return columns[0].rows.Count;
            }

            public void AddRow(string value)
            {
                columns[0].rows.Add(value);
                rowPictures.Add(null);
                rowTags.Add(null);

                for (int i = 1; i < columns.Count; ++i)
                {
                    columns[i].rows.Add("");
                }
            }

            public dynamic RowTag(int index)
            {
                return rowTags[index];
            }

            public void SetRowTag(int index, dynamic value)
            {
                rowTags[index] = value;
            }

            public Picture RowPicture(int index) => rowPictures[index];

            public void SetRowPicture(int index, Picture value)
            {
                rowPictures[index] = value;
                IsUsingImages = true;
            }

            public void RemoveRow(int index)
            {
                foreach (var column in columns)
                {
                    column.rows.RemoveAt(index);
                }
                rowPictures.RemoveAt(index);
                rowTags.RemoveAt(index);
            }

            public void RemoveAllRows()
            {
                foreach (var column in columns)
                {
                    column.rows.Clear();
                }
                rowPictures.Clear();
                rowTags.Clear();
            }

            public string CellValue(int columnIndex, int row)
            {
                return columns[columnIndex].rows[row];
            }

            public void CellValue(int columnIndex, int row, string value)
            {
                columns[columnIndex].rows[row] = value;
            }

            public void AddColumn()
            {
                var column = new DataSourceColumn();
                columns.Add(column);
                column.ResizeToRowCount(columns[0].rows.Count);
            }

            public void RemoveLastColumn()
            {
                columns.RemoveAt(columns.Count - 1);
            }

            public int Columns
            {
                get => columns.Count;
            }
        }

        private class MiddleAlignedNSTextField : NSTextFieldCell
        {
            public override CGRect TitleRectForBounds(CGRect theRect)
            {
                var titleFrame = base.TitleRectForBounds(theRect);

                var titleSize = this.AttributedStringValue.Size;

                titleFrame.Y = theRect.Y + (theRect.Size.Height - titleSize.Height) / 2.0f;

                return titleFrame;
            }

            public override void DrawInteriorWithFrame(CGRect cellFrame, NSView inView)
            {
                var titleRect = this.TitleRectForBounds(cellFrame);

                this.AttributedStringValue.DrawInRect(titleRect);
            }
        }

        private class TableDelegate : NSTableViewDelegate
        {
            WeakReference<ListBox> listBoxRef;
            DataSource dataSource;
            public int rowHeight;

            public TableDelegate(DataSource dataSource, ListBox listbox, int rowHeight)
            {
                this.dataSource = dataSource;
                this.listBoxRef = new WeakReference<ListBox>(listbox);
                this.rowHeight = rowHeight;
            }

            [Export("tableView:heightOfRow:")]
            public override nfloat GetRowHeight(NSTableView tableView, nint row)
            {
                return rowHeight;
            }

            public override NSView GetViewForItem(NSTableView tableView, NSTableColumn tableColumn, nint row)
            {
                var columnIndex = ((NSTableColumnEx)tableColumn).index;
                var cellIdentifier = columnIndex.ToString();

                // Get suitable recycled object
                NSTableCellView view = (NSTableCellView)tableView.MakeView(cellIdentifier, this);

                // If we could not get recycled object then we have to create new one.
                if (view == null)
                {
                    view = new NSTableCellView();
                    view.Identifier = cellIdentifier;
                    //view.WantsLayer = true;

                    int offset = 0;

                    if (columnIndex == 0 && dataSource.IsUsingImages)
                    {
                        NSImageView imageView = new NSImageView(new CGRect(0, 0, 18, tableView.RowHeight));
                        offset = 21;

                        view.ImageView = imageView;
                        view.AddSubview(imageView);
                    }


                    NSTextField textView = new NSTextField(new CGRect(offset,0, tableColumn.Width - offset, tableView.RowHeight));
                    textView.BackgroundColor = NSColor.Clear;
                    textView.Bordered = false;
                    textView.Selectable = false;
                    textView.Editable = false;
                      

                    view.TextField = textView;
                    view.AddSubview(textView);

                    textView.AutoresizingMask = NSViewResizingMask.WidthSizable;

                    textView.Cell = new MiddleAlignedNSTextField();

                    textView.StringValue = dataSource.CellValue(((NSTableColumnEx)tableColumn).index, (int)row);
                }

                // Finally we populated the recycled or new object with data
                view.TextField.StringValue = dataSource.CellValue(((NSTableColumnEx)tableColumn).index, (int)row);

                if (columnIndex == 0 && dataSource.IsUsingImages)
                {
                    view.ImageView.Image = dataSource.RowPicture((int)row).Handle;
                }


                return view;
            }



            /*[Export("tableView:dataCellForTableColumn:row:")]
            public override NSCell GetDataCell(NSTableView tableView, NSTableColumn tableColumn, nint row)
            {
                NSTextFieldCell cell = new NSTextFieldCell();


                cell.StringValue = dataSource.GetCellData((int)row) + "xxx";

                return cell;
            }*/

            public override void SelectionDidChange(NSNotification notification)
            {
                if (listBoxRef.TryGetTarget(out ListBox listBox))
                {
                    listBox.OnSelectionChanged();
                }
            }

            
        }

        private class NSTableColumnEx : NSTableColumn
        {
            public int index;

            public NSTableColumnEx(int index)
            {
                this.index = index;
            }
        }

        public ListBox(float x, float y, float width, float height, string[] columnTitles)
        {
            Handle = new NSScrollView(new CGRect(x, y, width, height))
            {
                ScrollerStyle = NSScrollerStyle.Legacy,
                AutohidesScrollers = false,
                HasVerticalScroller = true,
                
                
            };

            this.initialColumnTitles = columnTitles;

            var dataSource = new DataSource();
            dataSource.AddColumn();

            tableView = new NSTableView(new CGRect(x, y, width, height))
            {
                IntercellSpacing = new CGSize(width: 0, height: 2),
                //HeaderView = null,
                DataSource = dataSource,
                Delegate = new TableDelegate(dataSource, this, 22),
                 
            };


            //tableView.DoubleAction = new ObjCRuntime.Selector("ViewDetailsSelector");

            var column = new NSTableColumnEx(0);

            column.HeaderCell.Title = columnTitles != null && columnTitles.Any() ? columnTitles[0] : string.Empty;

            tableView.AddColumn(column);

            //column.Width = 300; //TODO figure this one out
            //column.SizeToFit();

            ((NSScrollView)Handle).DocumentView = tableView;


            tableView.ColumnAutoresizingStyle = NSTableViewColumnAutoresizingStyle.FirstColumnOnly;

            tableView.DoubleClick += TableView_DoubleClick;
        }

        private void TableView_DoubleClick(object sender, EventArgs e)
        {
            if (tableView.ClickedRow >= 0)
            { 
                OnDoubleClickRow((int)tableView.ClickedColumn, (int)tableView.ClickedRow);
            }
        }

        protected void OnDoubleClickRow(int columnIndex, int rowIndex)
        {
            DoubleClickRow?.Invoke(this, new CellEventArgs(columnIndex, rowIndex));
        }

        public void AddRow(string value)
        {
            ((DataSource)tableView.DataSource).AddRow(value);
            if (this.Visible)
            {
                tableView.ReloadData();
            }
        }

        public void RemoveRow(int rowIndex)
        {
            if(rowIndex < 0 || rowIndex >= tableView.RowCount)
            {
                throw new IndexOutOfRangeException();
            }

            ((DataSource)tableView.DataSource).RemoveRow(rowIndex);
            tableView.ReloadData();
        }

        public void RemoveAllRows()
        {
            ((DataSource)tableView.DataSource).RemoveAllRows();
            tableView.ReloadData();
        }

        public void SetRowPicture(int rowIndex, Picture picture)
        {
            if (rowIndex < 0 || rowIndex >= tableView.RowCount)
            {
                throw new IndexOutOfRangeException();
            }

            ((DataSource)tableView.DataSource).SetRowPicture(rowIndex, picture);
        }

        public void SetRowTag(int rowIndex, dynamic value)
        {
            if (rowIndex < 0 || rowIndex >= tableView.RowCount)
            {
                throw new IndexOutOfRangeException();
            }

            ((DataSource)tableView.DataSource).SetRowTag(rowIndex, value);
        }

        public dynamic RowTag(int rowIndex)
        {
            if (rowIndex < 0 || rowIndex >= tableView.RowCount)
            {
                throw new IndexOutOfRangeException();
            }

            return ((DataSource)tableView.DataSource).RowTag(rowIndex);
        }

        public string CellText(int columnIndex, int rowIndex)
        {
            if (rowIndex < 0 || rowIndex >= tableView.RowCount)
            {
                throw new IndexOutOfRangeException();
            }

            return ((DataSource)tableView.DataSource).CellValue(columnIndex, rowIndex);
        }

        public void SetCellText(int columnIndex, int rowIndex, string value)
        {
            if (rowIndex < 0 || rowIndex >= tableView.RowCount)
            {
                throw new IndexOutOfRangeException();
            }

            ((DataSource)tableView.DataSource).CellValue(columnIndex, rowIndex, value);
        }

        

        public int RowHeight
        {
            get { return ((TableDelegate)tableView.Delegate).rowHeight; }
            set { ((TableDelegate)tableView.Delegate).rowHeight = value; }
        }

        public int RowCount => (int)tableView.RowCount;

        protected void OnSelectionChanged()
        {
            SelectionChanged?.Invoke(this, EventArgs.Empty);
        }

        public int Columns
        {
            get { return ((DataSource)tableView.DataSource).Columns; }
            set
            {
                if(value >= 1 && value != ((DataSource)tableView.DataSource).Columns)
                {
                    while(value > ((DataSource)tableView.DataSource).Columns)
                    {
                        var column = new NSTableColumnEx(((DataSource)tableView.DataSource).Columns);
                        column.Title = (initialColumnTitles != null && initialColumnTitles.Length > ((DataSource)tableView.DataSource).Columns) ?
                            initialColumnTitles[((DataSource)tableView.DataSource).Columns] :
                            " ";

                        tableView.AddColumn(column);

                        ((DataSource)tableView.DataSource).AddColumn();
                    }

                    while (value < ((DataSource)tableView.DataSource).Columns)
                    {
                        tableView.RemoveColumn(tableView.TableColumns()[((DataSource)tableView.DataSource).Columns - 1]);
                        ((DataSource)tableView.DataSource).RemoveLastColumn();
                    }

                    
                }
            }
        }

        public int SelectedRow
        {
            get => (int)tableView.SelectedRow;
            set => tableView.SelectRow(value, false);
        }

        public int SelectedRowCount => (int)tableView.SelectedRowCount;

        public int[] SelectedRowIndexes
        {
            get
            {
                int[] result = new int[tableView.SelectedRowCount];
                int i = 0;

                var selection = tableView.SelectedRows;
                selection.EnumerateIndexes((nuint idx, ref bool stop) =>
                {
                    stop = false;
                    result[i++] = (int)idx;
                });

                return result;
            }
        }

        public bool Multiselection
        {
            get => tableView.AllowsMultipleSelection;
            set => tableView.AllowsMultipleSelection = value;
        }

        public bool AlternatingRowColors
        {
            get => tableView.UsesAlternatingRowBackgroundColors;
            set => tableView.UsesAlternatingRowBackgroundColors = value;
        }

        // Setter is is design time only
        public bool HasHeader
        {
            get => tableView.HeaderView != null;
            set
            {
                if (value == false)
                {
                    tableView.HeaderView = null;
                }
            }
        }

        // 
        // 
        // 
        public new bool Visible
        {
            get => base.Visible;
            set
            {
                tableView.ReloadData();
                base.Visible = value;
            } 
        }

        public override void Dispose()
        {
            tableView.DoubleClick -= TableView_DoubleClick;

            DoubleClickRow = null;
            SelectionChanged = null;
            tableView.Dispose();

            base.Dispose();
        }
    }
}

