﻿using System;
using System.Collections.Generic;
using System.Linq;
using AppKit;
using Foundation;

namespace Einhugur.Forms
{
    public class PagePanel : ContainerControl
    {
        public event EventHandler PageChanged;

        public class Page
        {
            readonly NSTabViewItem item;
            Func<IEnumerable<Control>> setupControls;

            public Page(Func<IEnumerable<Control>> setupControls = null)
            {
                item = new NSTabViewItem();
                this.setupControls = setupControls;

                item.View = new ContentViewEx();

            }
                       
            internal NSTabViewItem Item => item;

            internal Func<IEnumerable<Control>> SetupControlsProc => setupControls;

            internal void ClearSetupProc()
            {
                setupControls = null;
            }
        }

        private class TabDelegate : NSTabViewDelegate
        {
            readonly WeakReference<PagePanel> pagepanelControl;

            public TabDelegate(PagePanel pagepanelControl)
            {
                this.pagepanelControl = new WeakReference<PagePanel>(pagepanelControl);
            }

            [Export("tabView:didSelectTabViewItem:")]
            public override void DidSelect(NSTabView tabView, NSTabViewItem item)
            {
                if (pagepanelControl.TryGetTarget(out PagePanel ctrl))
                {
                    ctrl.OnPageSelected();
                }
            }
        }



        public PagePanel(float x, float y, float width, float height, Func<IEnumerable<Page>> setupPanels = null)
        {
            Handle = new NSTabView(new CoreGraphics.CGRect(x, y, width, height));
            var tabView = (NSTabView)Handle;
            tabView.BorderType = NSTabViewBorderType.None;
            tabView.TabViewType = NSTabViewType.NSNoTabsNoBorder;

            ((NSTabView)Handle).Delegate = new TabDelegate(this);

            if (setupPanels != null)
            {
                var pages = setupPanels();

                if (pages != null)
                {
                    int i = 0;

                    foreach (var page in pages)
                    {
                        i++;
                        ((NSTabView)Handle).Add(page.Item);


                        if (page.SetupControlsProc != null)
                        {
                            // We need to get valid size for all the tabs for layout logic to work.
                            if (i != 1)
                            {
                                page.Item.View.Frame = pages.First().Item.View.Frame;
                            }

                            SetupControlsForView(page.Item.View, page.SetupControlsProc);

                            page.ClearSetupProc();
                        }
                    }
                }
            }

        }

        public int SelectedPageIndex => Array.FindIndex(((NSTabView)Handle).Items, x => x.Equals(((NSTabView)Handle).Selected));

        protected virtual void OnPageSelected()
        {
            PageChanged?.Invoke(this, EventArgs.Empty);
        }

        public void SelectPage(int index)
        {
            ((NSTabView)Handle).SelectAt(index);
        }

        public override void Dispose()
        {
            PageChanged = null;

            base.Dispose();
        }
    }
}

