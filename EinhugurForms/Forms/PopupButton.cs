﻿using System;
using System.Diagnostics.CodeAnalysis;
using AppKit;
using CoreGraphics;
using Foundation;
using Einhugur.Arguments;
using Einhugur.Drawing;

namespace Einhugur.Forms
{
    [SuppressMessage("ReSharper", "UnusedMember.Local")]
    public class PopupButton : Control
    {
        public const int DefaultHeight = 20;
        public const int DefaultWidth = 100;

        public event EventHandler GotFocus;
        public event EventHandler LostFocus;
        public event EventHandler<ActionEventArgs> SelectionChanged;


        [SuppressMessage("ReSharper", "InconsistentNaming")]
        [SuppressMessage("ReSharper", "UnusedParameter.Local")]
        private sealed class NSPopUpButtonEx : NSPopUpButton
        {
            public nint lastSelectedIndex;
            readonly WeakReference<PopupButton> owner;

            public NSPopUpButtonEx(CoreGraphics.CGRect frameRect, PopupButton owner)
                : base(frameRect, false)
            {
                lastSelectedIndex = 0;
                Target = this;
                Action = new ObjCRuntime.Selector("selectionChanged:");

                this.owner = new WeakReference<PopupButton>(owner);
            }

            [Export("selectionChanged:")]
            public void selectionChanged(NSObject sender)
            {
                var selectedIndex = this.IndexOfSelectedItem;

                if (selectedIndex != lastSelectedIndex)
                {
                    lastSelectedIndex = selectedIndex;

                    if (owner.TryGetTarget(out PopupButton target))
                    {
                        target.OnSelectionChanged(true);
                    }
                }
            }

            public override bool BecomeFirstResponder()
            {
                if(owner.TryGetTarget(out PopupButton btn))
                {
                    btn.OnGotFocus();
                }

                return base.BecomeFirstResponder();
            }

            public override bool ResignFirstResponder()
            {
                if (owner.TryGetTarget(out PopupButton btn))
                {
                    btn.OnLostFocus();
                }

                return base.ResignFirstResponder();
            }
        }

        public PopupButton(float x, float y) : this(x, y, DefaultWidth, DefaultHeight)
        { }

        public PopupButton(float x, float y, float width, float height)
        {
            Handle = new NSPopUpButtonEx(new CoreGraphics.CGRect(x, y, width, height), this);
        }

        protected virtual void OnSelectionChanged(bool invokedByUser)
        {
            SelectionChanged?.Invoke(this, new ActionEventArgs(invokedByUser));
        }

        public int SelectedIndex
        {
            get => (int)((NSPopUpButtonEx)Handle).IndexOfSelectedItem;
            set
            {
                ((NSPopUpButtonEx)Handle).SelectItem(value);
                ((NSPopUpButtonEx)Handle).lastSelectedIndex = value;

                OnSelectionChanged(false);
            }
        }

        public bool Enabled
        {
            get => ((NSPopUpButton)Handle).Enabled;
            set => ((NSPopUpButton)Handle).Enabled = value;
        }

        public string SelectedTitle => ((NSPopUpButtonEx)Handle).TitleOfSelectedItem;

        public void AddItem(string title)
        {
            ((NSPopUpButtonEx)Handle).AddItem(title);
        }

        public void AddItems(string[] titles)
        {
            ((NSPopUpButtonEx)Handle).AddItems(titles);
        }

        public void RemoveItem(int index)
        {
            ((NSPopUpButtonEx)Handle).RemoveItem(index);
        }

        public void RemoveAllItems()
        {
            ((NSPopUpButtonEx)Handle).RemoveAllItems();
        }
        
        public string ToolTip
        {
            get => ((NSPopUpButtonEx) Handle).ToolTip;
            set => ((NSPopUpButtonEx) Handle).ToolTip = value;
        }

        public string[] Items
        {
            get => ((NSPopUpButtonEx)Handle).ItemTitles();
            set
            {
                bool selectionChanged = (int)((NSPopUpButtonEx)Handle).ItemCount > 0 && ((NSPopUpButtonEx)Handle).IndexOfSelectedItem >= 0;

                if (((NSPopUpButtonEx)Handle).ItemCount > 0)
                {
                    ((NSPopUpButtonEx)Handle).RemoveAllItems();
                }
                ((NSPopUpButtonEx)Handle).AddItems(value);

                if (selectionChanged)
                {
                    OnSelectionChanged(false);
                }
            }
        }

        public int Count => (int)((NSPopUpButtonEx)Handle).ItemCount;

        protected virtual void OnGotFocus()
        {
            GotFocus?.Invoke(this, EventArgs.Empty);
        }

        protected virtual void OnLostFocus()
        {
            LostFocus?.Invoke(this, EventArgs.Empty);
        }
        
        public override void DrawInto(GraphicsContext g, float x, float y)
        {
            NSPopUpButton btn = new NSPopUpButton(new CGRect(0, 0, this.Width, this.Height), false);
            btn.WantsLayer = true;
            btn.Enabled = this.Enabled;

            if (this.SelectedIndex >= 0)
            {
                // We only have to add the selected item to render the control
                btn.AddItem(this.SelectedTitle);
                btn.SelectItem(0);
            }
            
            g.Handle.SaveGraphicsState();
            var tr = new NSAffineTransform();
            tr.Translate(x, y);
            tr.Concat();
            
            
            btn.DisplayRectIgnoringOpacity(new CGRect(0, 0, this.Width, this.Height), g.Handle);

            g.Handle.RestoreGraphicsState();
        }

        public override void Dispose()
        {
            GotFocus = null;
            LostFocus = null;
            SelectionChanged = null;

            base.Dispose();
        }

    }
}

