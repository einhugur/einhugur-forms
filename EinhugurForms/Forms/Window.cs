﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using AppKit;
using Einhugur.Drawing;
using CoreGraphics;
using Einhugur.Arguments;
using Einhugur.Menus;
using Einhugur.Forms;
using System.Linq;

namespace Einhugur.Forms
{
    [Flags]
    public enum WindowStyle : ulong
    {
        Borderless = NSWindowStyle.Borderless,
        HasCloseButton = NSWindowStyle.Closable,
        Resizable = NSWindowStyle.Resizable,
        Titled = NSWindowStyle.Titled,
        HasMinimizeButton = NSWindowStyle.Miniaturizable,
        FullScreenWindow = NSWindowStyle.FullScreenWindow
    }

    public enum WindowPlacement
    {
        Manual,
        Center
    }

    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "MemberCanBeProtected.Global")]
    [SuppressMessage("ReSharper", "PublicConstructorInAbstractClass")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public abstract partial class Window
    {

        public class WindowCreationParameters
        {
            public float Left;
            public float Top;
            public float Width;
            public float Height;
            public WindowStyle Style;
            public string Title;

            public nfloat MaxWidth;
            public nfloat MaxHeight;

            public nfloat MinWidth;
            public nfloat MinHeight;

            public Dictionary<MenuItem, EventHandler<MenuEventArgs>> MenuHandlers;

            public bool MouseTracking;
            public WindowPlacement InitialPlacement;
        }

        private bool hasBeenClosed;
        private readonly NSWindow window;
        private Toolbar pToolbar;

        public Window()
        {
            var prm = new WindowCreationParameters()
            {
                Left = 50,
                Top = 50,
                Width = 500,
                Height = 500,
                Style = WindowStyle.HasCloseButton | WindowStyle.Titled | WindowStyle.Resizable,
                Title = "Untitled",
                MaxHeight = nfloat.MaxValue,
                MaxWidth = nfloat.MaxValue,
                MinHeight = 28,
                MinWidth = 0,
                MouseTracking = false,
                InitialPlacement = WindowPlacement.Manual
            };

            SetupWindow(prm);

            window = new FrameworkWindow(new CGRect(prm.Left, NSScreen.MainScreen.Frame.Height - prm.Top - prm.Height, prm.Width, prm.Height),
                (NSWindowStyle)prm.Style,
                prm.InitialPlacement,
                prm.MenuHandlers,
                ValidateMenuItem,
                WillClose,
                Resizing,
                this);

            window.MaxSize = new CGSize(prm.MaxWidth, prm.MaxHeight);
            window.MinSize = new CGSize(prm.MinWidth, prm.MinHeight);
            window.Title = prm.Title;

            window.ContentViewController = new FrameworkViewController(
                new CGRect(0, 0, prm.Width, prm.Height),
                ViewLoaded,
                this,
                prm.MouseTracking);


        }

        public NSView View => window.ContentView;

        public NSWindow Handle => window;

        public WindowPlacement InitialPlacement
        {
            get => ((FrameworkWindow)window).InitialPlacement;
        }

        internal bool HasBeenClosed => hasBeenClosed;

        public bool MouseTracking
        {
            get => ((NSViewEx) window.ContentView).mouseTracking;
            set => ((NSViewEx) window.ContentView).mouseTracking = value;
        }

        protected abstract void SetupWindow(WindowCreationParameters setupParameters);


        protected virtual void Disposing()
        {
            
        }

        protected virtual void Opening()
        {

        }

        protected virtual void Closing()
        {

        }

        protected virtual void Paint(GraphicsContext g)
        {

        }
        
        protected virtual void MouseWheel(nfloat x, nfloat y, nfloat scrollingDeltaX, nfloat scrollingDeltaY, bool hasPreciseScrollingDeltas)
        {
            
        }

        protected virtual void MouseDown(nfloat x, nfloat y, MouseButton button)
        {

        }

        protected virtual void MouseUp(nfloat x, nfloat y, MouseButton button)
        {

        }

        protected virtual void MouseEntered()
        {
            
        }
        
        protected virtual void MouseExited()
        {
            
        }
        
        protected virtual void MouseMoved(nfloat x, nfloat y)
        {
            
        }

        protected virtual void Resizing()
        {

        }

        protected virtual void Activating()
        {
            
        }

        protected virtual void Deactivating()
        {
            
        }

        private void WillClose()
        {
            Closing();
            Disposing();
            hasBeenClosed = true;
        }

        internal void RemoveControl(Control control)
        {
            ((FrameworkWindow)window).controls.Remove(control);
        }

        public void AddControl(Control control)
        {
            AddControl(control, View);
        }

        private void AddControl(Control control, NSView view)
        {
            ((FrameworkWindow)window).controls.Add(control);

            view.AddSubview(control.Handle);
            control.SetParentWindow(this);

            if (control.LockLeft && control.LockRight)
            {
                control.Handle.AutoresizingMask |= NSViewResizingMask.WidthSizable;
            }
            else if (control.LockRight)
            {
                control.Handle.AutoresizingMask |= NSViewResizingMask.MinXMargin;
            }

            if (control.LockTop && control.LockBottom)
            {
                control.Handle.AutoresizingMask |= NSViewResizingMask.HeightSizable;
            }
            else if (control.LockBottom)
            {
                control.Handle.AutoresizingMask |= NSViewResizingMask.MinYMargin;
            }
        }

        protected void ViewLoaded(NSView view)
        {
            ((FrameworkWindow)window).SetupEvents();
            
            var controls = SetupControls();

            view.TranslatesAutoresizingMaskIntoConstraints = true;

            foreach (var control in controls)
            {
                AddControl(control, view);
            }

            Opening();
        }

        protected virtual IEnumerable<Control> SetupControls()
        {
            return new Control[] { };
        }

        public string Title
        {
            get => window.Title;
            set => window.Title = value;
        }

        public nfloat MaxWidth
        {
            get => window.MaxSize.Width;
            set => window.MaxSize = new CGSize(value, window.MaxSize.Height);
        }
        
        public nfloat MaxHeight
        {
            get => window.MaxSize.Height;
            set => window.MaxSize = new CGSize(window.MaxSize.Width, value);
        }
        
        public nfloat MinWidth
        {
            get => window.MinSize.Width;
            set => window.MinSize = new CGSize(value, window.MinSize.Height);
        }
        
        public nfloat MinHeight
        {
            get => window.MinSize.Height;
            set => window.MinSize = new CGSize(window.MinSize.Width, value);
        }

        public nfloat Left
        {
            get => window.Frame.Left;
            set =>window.SetFrame(new CGRect(value, window.Frame.Top, window.Frame.Width, window.Frame.Height), true);
        }

        public nfloat Top
        {
            get => window.Screen.Frame.Height - window.Frame.Top;
            set => window.SetFrame(new CGRect(window.Frame.Left, window.Screen.Frame.Height - value, window.Frame.Width, window.Frame.Height), true);
        }

        public nfloat Width
        {
            get => window.Frame.Width;
            set => window.SetFrame(new CGRect(window.Frame.Left, window.Frame.Top, value, window.Frame.Height), true); 
        }

        public nfloat Height
        {
            get => window.Frame.Height;
            set => window.SetFrame(new CGRect(window.Frame.Left, window.Frame.Top, window.Frame.Width, value), true); 
        }

        public nfloat TitleBarHeight
        {
            get => window.Frame.Height - window.ContentLayoutRect.Height;
        }
                 
        public FileSystemItem RepresentedFile
        {
            get
            {
                var file = window.RepresentedFilename;

                return file != null ? new FileSystemItem(file) : null;
            }
            set
            {
                window.SetTitleWithRepresentedFilename(value != null  ? value.FullPath : null);
            }
        }

        public Toolbar Toolbar
        {
            get => this.pToolbar;
            set {
                this.pToolbar = value;
                window.Toolbar = value;
            }
        }

        public virtual bool ValidateMenuItem(NSMenuItem menuItem)
        {
            return true;
        }

        public void Show()
        {
            window.MakeKeyAndOrderFront(null);
        }

        public void Close()
        {
            window.Close();
            hasBeenClosed = true;
        }
    }
}

