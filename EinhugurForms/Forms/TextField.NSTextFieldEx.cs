using System;
using AppKit;
using Einhugur.Arguments;
using Foundation;

namespace Einhugur.Forms
{
    public partial class TextField : Control
    {
        private class NSTextFieldEx : NSTextField
        {
            private readonly WeakReference<TextField> owner;
            private NSObject watcher;
            private bool hasFocus;

            public NSTextFieldEx(CoreGraphics.CGRect frameRect, TextField textField)
                : base(frameRect)
            {
                owner = new WeakReference<TextField>(textField);
            }

            private NSEvent KeyFilter(NSEvent theEvent)
            {
                if (owner.TryGetTarget(out var textField))
                {
                    if (textField.OnKeyDown(theEvent.KeyCode, theEvent.Characters, theEvent.CharactersIgnoringModifiers, (KeyModifierFlags)theEvent.ModifierFlags))
                    {
                        return null;
                    }
                }
                
                return theEvent;
            }

            public override bool BecomeFirstResponder()
            {
                hasFocus = true;
                
                if (owner.TryGetTarget(out var textField))
                {
                    if (textField.KeyDown != null)
                    {
                        watcher = NSEvent.AddLocalMonitorForEventsMatchingMask(NSEventMask.KeyDown, KeyFilter);
                    }
                    
                    textField.OnGotFocus();
                }
                
                return base.BecomeFirstResponder();
            }

            public void Deactivate()
            {
                // Pull down the key down filter.
                if (hasFocus && watcher != null)
                {
                    NSEvent.RemoveMonitor(watcher);
                    watcher = null;
                }
            }
            
            public void Activate()
            {
                // Restore the Key down filter if needed
                if (hasFocus && watcher == null)
                {
                    if (owner.TryGetTarget(out var textField))
                    {
                        if (textField.KeyDown != null)
                        {
                            watcher = NSEvent.AddLocalMonitorForEventsMatchingMask(NSEventMask.KeyDown, KeyFilter);
                        }
                    }
                }
            }
            
            public override void DidEndEditing(NSNotification notification)
            {
                if (watcher != null)
                {
                    NSEvent.RemoveMonitor(watcher);
                    watcher = null;
                }
                
                base.DidEndEditing(notification);

                hasFocus = false;
                
                if (owner.TryGetTarget(out var textField))
                {
                    textField.OnLostFocus();
                }
            }

            public override void KeyUp(NSEvent theEvent)
            {
                if (owner.TryGetTarget(out var textField))
                {
                    if (textField.OnKeyUp(theEvent.KeyCode, theEvent.Characters, theEvent.CharactersIgnoringModifiers, (KeyModifierFlags)theEvent.ModifierFlags))
                    {
                        return;
                    }
                }

                base.KeyUp(theEvent);
            }
        }
    }
}