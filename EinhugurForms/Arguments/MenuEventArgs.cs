﻿using System;
using Einhugur.Menus;

namespace Einhugur.Arguments
{

    public class MenuEventArgs : EventArgs
    {
        private bool handled;

        public MenuEventArgs()
        {
            this.handled = false;
        }

        public bool Handled
        {
            get { return handled; }
            set { handled = value; }
        }
    }

    public class SpecialMenuEventArgs : EventArgs
    {
        private SpecialMenuItem.SpecialCommand command;

        public SpecialMenuEventArgs(SpecialMenuItem.SpecialCommand command)
        {
            this.command = command;
        }

        public SpecialMenuItem.SpecialCommand Command => command;
    }

    public class SpecialMenuEventEnablerArgs : SpecialMenuEventArgs
    {
        private bool enabled;

        public SpecialMenuEventEnablerArgs(SpecialMenuItem.SpecialCommand command)
            : base(command)
        {
            this.enabled = false;
        }

        public bool Enabled
        {
            get => enabled;
            set => enabled = value;
        }

    }

}

