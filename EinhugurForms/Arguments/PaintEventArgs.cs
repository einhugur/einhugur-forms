﻿using System;
using Einhugur.Drawing;

namespace Einhugur.Arguments
{
    public class PaintEventArgs : EventArgs
    {
        private readonly GraphicsContext graphics;

        public PaintEventArgs(GraphicsContext g)
        {
            graphics = g;
        }

        public GraphicsContext Graphics => graphics;
    }
}

