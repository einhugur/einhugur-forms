﻿
using Foundation;

namespace Einhugur
{
    public class Localized
    {
        public static string Text(string key, string defaultValue)
        {
            return NSBundle.MainBundle.GetLocalizedString(key, defaultValue);
        }
    }
}

