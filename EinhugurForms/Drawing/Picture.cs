﻿using System;
using AppKit;

namespace Einhugur.Drawing
{
    public class Picture
    {
        private readonly NSImage image;
        private readonly NSBitmapImageRep rep;

        private Picture(NSImage image)
        {
            this.image = image;
        }

        public Picture(int width, int height)
        {
            //rep = new NSBitmapImageRep()

            //image = new NSImage(new CoreGraphics.CGSize((double)width, (double)height));

            //image.Representations[0].

            rep = new NSBitmapImageRep(
                IntPtr.Zero,
                width,
                height,
                8,
                4,
                true,
                false,
                NSColorSpace.CalibratedRGB,
                0,
                0);

            image = new NSImage(new CoreGraphics.CGSize(width, height));
            image.AddRepresentation(rep);
        }

        public GraphicsContext Graphics => rep == null ? null : new GraphicsContext(rep);

        public static Picture FromAsset(string asset)
        {
            return new Picture(NSImage.ImageNamed(asset));
        }

        public NSImage Handle => image;

        public int Width => (int)image.Size.Width;
        public int Height => (int)image.Size.Height;


    }
}

