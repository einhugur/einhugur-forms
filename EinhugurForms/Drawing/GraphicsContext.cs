﻿using System;
using System.Collections.Generic;
using AppKit;
using CoreGraphics;
using Foundation;

namespace Einhugur.Drawing
{
    public class GraphicsContext : IDisposable
    {
        private class GraphicsState
        {
            public Color fillColor;
            public Color strokeColor;
            public Font font;
        };

        readonly NSGraphicsContext context;
        readonly CGContext cgContext;
        Color fillColor;
        Color strokeColor;
        Font font;
        CGSize size;
        private readonly bool isOffScreen;

        Stack<GraphicsState> states;

        public GraphicsContext(NSBitmapImageRep bitmap)
        {
            isOffScreen = true;
            context = NSGraphicsContext.FromBitmap(bitmap);
            
            cgContext = context.CGContext;
            
            size = bitmap.Size;

            NSGraphicsContext old = NSGraphicsContext.CurrentContext;
            if (context.Handle != old?.Handle)
            {
                NSGraphicsContext.CurrentContext = context;
            }
            
            strokeColor = fillColor = new Color(new CGColor(0.0f, 0.0f, 0.0f));
        }

        public GraphicsContext(NSGraphicsContext ctx, CGSize size)
        {
            isOffScreen = false;
            this.context = ctx;

            cgContext = context.CGContext;

            this.size = size;

            strokeColor = fillColor = new Color(new CGColor(0.0f, 0.0f, 0.0f));

        }

        public int Width => (int)size.Width;
        public int Height => (int)size.Height;

        public void Save()
        {
            if (states == null)
            {
                states = new Stack<GraphicsState>();
            }

            var state = new GraphicsState()
            {
                fillColor = this.fillColor,
                strokeColor = this.strokeColor,
                font = this.font
            };

            states.Push(state);

            context.SaveGraphicsState();
        }

        public void Restore()
        {
            if (states != null && states.Count > 0)
            {
                var state = states.Pop();

                fillColor = state.fillColor;
                strokeColor = state.strokeColor;
                font = state.font;
            }

            context.RestoreGraphicsState();
        }

        public void Clip(float x, float y, float width, float height)
        {
            cgContext.ClipToRect(new CGRect(x, y, width, height));
        }

        public void Scale(float x, float y)
        {
            cgContext.ScaleCTM(x,y);
        }

        public void Rotate(float angle)
        {
            cgContext.RotateCTM(angle);
            
        }
        
        public void Rotate(float angle, float x, float y)
        {
            //TODO: Verify if this is correct?
            cgContext.TranslateCTM(x,y);
            cgContext.RotateCTM(angle);
            cgContext.TranslateCTM(-x,-y);
        }

        public void DrawLine(float x1, float y1, float x2, float y2)
        {
            cgContext.MoveTo(x1, y1);
            cgContext.AddLineToPoint(x2, y2);
            cgContext.StrokePath();
        }
        
        public void FillRoundRectangle(float x, float y, float width, float height, float xRadius, float yRadius)
        {
            NSGraphicsContext old = NSGraphicsContext.CurrentContext;

            if (context.Handle != old?.Handle)
            {
                NSGraphicsContext.CurrentContext = context;
            }

            NSBezierPath path = NSBezierPath.FromRoundedRect(new CGRect(x, y, width, height), xRadius, yRadius);

            path.Fill();

            if (context.Handle != old?.Handle)
            {
                NSGraphicsContext.CurrentContext = old;
            }
        }

        public void DrawRoundRectangle(float x, float y, float width, float height, float xRadius, float yRadius)
        {
            NSGraphicsContext old = NSGraphicsContext.CurrentContext;

            if (context.Handle != old?.Handle)
            {
                NSGraphicsContext.CurrentContext = context;
            }

            NSBezierPath path = NSBezierPath.FromRoundedRect(new CGRect(x, y, width, height), xRadius, yRadius);

            path.Stroke();

            if (context.Handle != old?.Handle)
            {
                NSGraphicsContext.CurrentContext = old;
            }
        }

        public void FillRectangle(float x, float y, float width, float height)
        {
            cgContext.FillRect(new CGRect(x, y, width, height));
        }

        public void DrawRectangle(float x, float y, float width, float height)
        {
            cgContext.StrokeRect(new CGRect(x, y, width, height));
        }

        public void FillOval(float x, float y, float width, float height)
        {
            cgContext.FillEllipseInRect(new CGRect(x, y, width, height));
        }

        public void DrawOval(float x, float y, float width, float height)
        {
            cgContext.StrokeEllipseInRect(new CGRect(x, y, width, height));
        }

        public Font Font
        {
            get => font;
            set
            {
                font = value;
                //font.AsNSFont.SetInContext(context);
            }
        }

        public Color StrokeColor
        {
            get => strokeColor;
            set
            {
                if (value != null)
                {
                    cgContext.SetStrokeColor(value.AsCGColor);
                    strokeColor = value;
                }
            }
        }

        public void DrawPicture(Picture picture, double x, double y)
        {
            NSGraphicsContext old = NSGraphicsContext.CurrentContext;



            if (context.Handle != old?.Handle)
            {
                NSGraphicsContext.CurrentContext = context;
            }

            if (picture != null)
            {
                picture.Handle.Draw(new CGRect(x, y, picture.Width, picture.Height), CGRect.Empty, NSCompositingOperation.SourceOver, 1f);
            }

            if (old?.Handle != context.Handle)
            {
                NSGraphicsContext.CurrentContext = old;
            }
        }

        public Color FillColor
        {
            get => fillColor;
            set
            {
                if (value != null)
                {
                    cgContext.SetFillColor(value.AsCGColor);
                    fillColor = value;
                }
            }
        }

        public void DrawString(string text, nfloat x, nfloat y, double width = 0.0f)
        {
            NSGraphicsContext old = NSGraphicsContext.CurrentContext;

            if (context.Handle != old?.Handle)
            {
                NSGraphicsContext.CurrentContext = context;
            }

            if (isOffScreen)
            {
                context.SaveGraphicsState();
                var tr = new NSAffineTransform();
                tr.Translate(0, size.Height);
                tr.Scale(1, -1);
                tr.Concat();
            }

            var attributedString = new NSAttributedString(
                text,
                foregroundColor: strokeColor.AsNSColor,
                font: font.AsNSFont);

            
            
            if (width != 0.0)
            {
                var bounds = attributedString.BoundingRectWithSize(new CGSize(width, 20000.0f), NSStringDrawingOptions.UsesLineFragmentOrigin);
                bounds.Offset(x, y);

                attributedString.DrawString(bounds, NSStringDrawingOptions.UsesLineFragmentOrigin | NSStringDrawingOptions.UsesDeviceMetrics);
            }
            else
            {
                attributedString.DrawString(new CGPoint(x, y));
            }
            
            if (isOffScreen)
            {
                context.RestoreGraphicsState();
            }

            if (old?.Handle != context.Handle)
            {
                NSGraphicsContext.CurrentContext = old;
            }
        }



        public void DrawString(string text, nfloat x, nfloat y, nfloat width, nfloat height)
        {
            NSGraphicsContext old = NSGraphicsContext.CurrentContext;

            if (context.Handle != old?.Handle)
            {
                NSGraphicsContext.CurrentContext = context;
            }
            
            if (isOffScreen)
            {
                context.SaveGraphicsState();
                var tr = new NSAffineTransform();
                tr.Translate(0, size.Height);
                tr.Scale(1, -1);
                tr.Concat();
            }

            var attributedString = new NSAttributedString(
                text,
                foregroundColor: strokeColor.AsNSColor,
                font: font.AsNSFont);

            attributedString.DrawString(new CGRect(x, y, width, height));

            if (isOffScreen)
            {
                context.RestoreGraphicsState();
            }
            
            if (old?.Handle != context.Handle)
            {
                NSGraphicsContext.CurrentContext = old;
            }
        }

        public double StringWidth(string text)
        {
            var attributedString = new NSAttributedString(
                text,
                font: font.AsNSFont);
            
            return attributedString.Size.Width;
        }

        public double StringHeight(string text)
        {
            var attributedString = new NSAttributedString(
                text,
                font: font.AsNSFont);

            return attributedString.Size.Height;
        }

        public void Dispose()
        {
            font = null;
            context.Dispose();
            cgContext.Dispose();
        }

        public NSGraphicsContext Handle => context;
    }
}

