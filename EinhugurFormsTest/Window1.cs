﻿using System;
using Darwin;
using Einhugur.Forms;
using Einhugur.Drawing;
using Einhugur.Arguments;
using Einhugur.Dialogs;

namespace TestApp
{
    public partial class Window1 : Window
    {
        //private int counter;

        protected override void Opening()
        {
            // Testing Appearance setter
            /*Application.Instance.Appearance = ApplicationAppearance.Dark;
            
            if (Application.Instance.Appearance == ApplicationAppearance.SystemDefault)
            {
                MessageBox.Show("Default appearance");
            }
            else if (Application.Instance.Appearance == ApplicationAppearance.Dark)
            {
                MessageBox.Show("Dark mode");
            }*/
            //progressBar.StartIntermediate();
            timer1.Start();
        }
        

        private void QuitMenuItem_Click(object sender, MenuEventArgs e)
        {
            MessageBox.Show("We get this messagebox since the Window subscribed the Quit, but did not mark it as handled event is then forwarded to the App class");

            //If we wanted to not let the Event propagate to the App then we would mark it as handled
            //e.Handled = true;
        }

        Button btn;

        private void Button4_Pressed(object sender, EventArgs e)
        {
            btn = new Button(30, 140, 112, 30)
            {
                Caption = "Test embed"
            };
            btn.Pressed += Btn_Pressed;

            AddControl(btn);

            //var window = new Window2();

            //window.Show();
        }

        private void Btn_Pressed(object sender, EventArgs e)
        {
            btn.Close();
        }

        private void Button5_Pressed(object sender, EventArgs e)
        {
            var window = new Window3();

            window.Show();
        }
        
        private void Button8_Pressed(object sender, EventArgs e)
        {
            var window = new Window4();

            window.Show();
        }

        private void Button7_Pressed(object sender, EventArgs e)
        {
            Application.ShowURL("https://www.einhugur.com");
        }
        
        private void Button6_Pressed(object sender, EventArgs e)
        {
            var dlg = new MessageBox()
            {
                Message = "What do you want to do with the bowl on the table?",
                Information = "Bowl might get damaged if it crashes hard on the floor."
            };
            dlg.AddButton("Gently move the bowl");
            dlg.AddButton("Bump off table").DestructiveAction = true;
            dlg.AddButton("Leave the bowl as is").IsCancelButton = true;

            var result = dlg.ShowModal(this);
            
            MessageBox.Show($"Button with index {result} was clicked.");
        }

        private void ComboButton1_Pressed(object sender, EventArgs e)
        {

            MessageBox.Show($"Combo button clicked");

            comboButton1.MenuEnabled = true;
        }

        private void Item1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Item 1 was clicked");

            comboButton1.MenuEnabled = false;
        }

        private void Item2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Item 2 was clicked");
        }

        private void Item3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Item 3 was clicked");
        }

        private void Button1_Pressed(object sender, EventArgs e)
        {
            //MessageBox.Show("We have a success!!");

            var f = OpenFileDialog.Show(new[] { "svg" });
            //var f = SaveFileDialog.Show(new[] { "svg" });
            //var f = SelectDirectoryDialog.Show("Select directory");

            if (f != null)
            { 
                MessageBox.Show(f.FullPath);

                this.RepresentedFile = null;
            }

            popupButton1.SelectedIndex = 1;

            
        }

        private void PopupButton1_SelectionChanged(object sender, EventArgs e)
        {
            MessageBox.Show($"Selected item with index number {popupButton1.SelectedIndex}");
        }

        private void Button2_Pressed(object sender, EventArgs e)
        {
            textField1.Text = "This is some test";
        }

        private void Button3_Pressed(object sender, EventArgs e)
        {
            MessageBox.Show("Hello from the button on the Canvas");
            checkbox1.Value = CheckBoxState.Unchecked;
        }


        private void TextField1_GotFocus(object sender, EventArgs e)
        {
            textField1.Text = "GotFocus";
        }

        private void Checkbox1_Action(object sender, ActionEventArgs e)
        {
            textField1.Text = "Checkbox was toggled by " + (e.InvokedByUser ? "user" : "code");
        }

        private void RadioButton1_Action(object sender, ActionEventArgs e)
        {
            MessageBox.Show("Option1 was toggled by " + (e.InvokedByUser ? "user" : "code"));
        }

        private void RadioButton2_Action(object sender, ActionEventArgs e)
        {
            MessageBox.Show("Option2 was toggled by " + (e.InvokedByUser ? "user" : "code"));
        }



        private void Canvas1_Paint(object sender, PaintEventArgs e)
        {
            var g = e.Graphics;

            g.FillColor = Color.RGB(0.2f, 0.2f, 0.2f);
            g.FillRectangle(10, 10, 230, 50);

            g.FillColor = Color.RGBA(1.0f, 0.1f, 0.1f, 0.9f);
            g.FillRectangle(30, 47, 190, 2);

            g.StrokeColor = Color.RGB(0.2f, 1.0f, 0.2f);
            g.Font = Font.SystemFont(Font.SystemFontSizeForControl(Font.SystemControlFontSize.Small));
            g.DrawString("Test test test test test test", 10.0f, 10.0f, 50);

            g.FillColor = Color.RGB(0.2f, 0.2f, 1.0f);
            g.FillRoundRectangle(280, 30, 50, 50, 8, 8);

            g.StrokeColor = Color.RGB(0.8f, 0.0f, 0.0f);
            g.DrawRoundRectangle(280, 30, 50, 50, 8, 8);

            // Testing off screen drawing into Picture object
            var p = new Picture(60, 60);
            var g2 = p.Graphics;
            g2.FillColor = Color.RGB(1.0f, 0.0f, 0.0f);
            g2.FillOval(0, 0, p.Width, p.Width);

            g2.StrokeColor = Color.RGB(0.0f, 0.0f, 0.0f);
            
            g2.Font = Font.SystemFont(Font.SystemFontSizeForControl(Font.SystemControlFontSize.Small));
            g2.Scale(2.0f,2.0f);
            g2.DrawString("Test", 10.0f, 20.0f);
            g2.Scale(0.5f,0.5f);
            
            g2.FillColor = Color.RGB(0.0f, 0.0f, 0.0f);
            //g2.FillRoundRectangle(0f,0f, 30f, 30f, 8f, 8f);
            g2.FillRectangle(0f,0f, 30f, 30f);
            
            g.DrawPicture(p, 195 , 40);
            

            //Testing drawing picture asset from the Apps asset catalog
            var p2 = Picture.FromAsset("AppIcon");
            g.DrawPicture(p2, 250, 10);
            
            g.DrawLine(0,0, g.Width, g.Height);
            g.DrawLine(0,g.Height, g.Width, 0);
        }

        private void Timer1_Action(object sender, EventArgs e)
        {
            //counter++;

            progressBar.Value++;

            if (progressBar.Value >= progressBar.MaxValue)
            {
                timer1.Stop();
                //progressBar.StopIntermediate();
            }

            //button2.Caption = counter.ToString();
        }

        private void Canvas1_MouseDown(object sender, MouseClickEventArgs e)
        {
            MessageBox.Show($"Canvas mouse down x:{e.X} y:{e.Y} Button:{e.Button.ToString()}");
        }

        protected override void Closing()
        {
            //MessageBox.Show("This window is closing");

            timer1.Dispose();
        }
    }
}

