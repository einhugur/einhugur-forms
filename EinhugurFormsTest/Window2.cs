﻿using System;
using Einhugur.Arguments;
using Einhugur.Dialogs;
using Einhugur.Forms;

namespace TestApp
{
    public partial class Window2 : Window
    {
        private void RadioButton1_Action(object sender, ActionEventArgs e)
        {
            MessageBox.Show("Option1 was toggled by " + (e.InvokedByUser ? "user" : "code"));
        }

        private void RadioButton2_Action(object sender, ActionEventArgs e)
        {
            MessageBox.Show("Option2 was toggled by " + (e.InvokedByUser ? "user" : "code"));
        }

        private void RadioButton3_Action(object sender, ActionEventArgs e)
        {
            MessageBox.Show("Option1 in box2 was toggled by " + (e.InvokedByUser ? "user" : "code"));
        }

        private void RadioButton4_Action(object sender, ActionEventArgs e)
        {
            MessageBox.Show("Option2 in box2 was toggled by " + (e.InvokedByUser ? "user" : "code"));
        }

        private void TabControl1_TabSelected(object sender, EventArgs e)
        {
            MessageBox.Show($"Selected tab index is: {tabControl1.SelectedTabIndex}");
        }
    }
}

