﻿using System;
using Einhugur.Arguments;
using Einhugur.Dialogs;
using Einhugur.Drawing;

namespace TestApp
{
    public partial class Window3
    {
        protected override void Opening()
        {
            for(var i = 0; i < 50; ++i)
            {
                listBox1.AddRow($"Test {i}");
                listBox1.SetCellText(1, listBox1.RowCount - 1, $"2nd column - {i}");
                listBox1.SetRowPicture(listBox1.RowCount - 1, Picture.FromAsset("AppIcon"));
            }
        }

        private void ListBox1_SelectionChanged(object sender, EventArgs e)
        {
            if(listBox1.SelectedRow >= 0)
            {
                label1.Caption = $"Selected row index is {listBox1.SelectedRow} value is {listBox1.CellText(0, listBox1.SelectedRow)}";
            }
            else
            {
                label1.Caption = "No row is selected"; 
            }
        }

        private void Button1_Pressed(object sender, EventArgs e)
        {
            var selection = listBox1.SelectedRowIndexes;

            MessageBox.Show(String.Join(",", selection));
        }

        private void Scrollbar1OnValueChanged(object sender, ActionEventArgs e)
        {
            label2.Caption = $"Vertical Scrollbar value is: {scrollbar1.Value}";
        }
        
        private void Scrollbar2OnValueChanged(object sender, ActionEventArgs e)
        {
            label2.Caption = $"Horizontal Scrollbar value is: {scrollbar2.Value}";
        }
    }
}

