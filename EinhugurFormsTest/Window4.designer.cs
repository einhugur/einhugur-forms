﻿using System;
using System.Collections.Generic;
using Einhugur.Forms;
using static Einhugur.Forms.Window;

namespace TestApp
{
    public partial class Window4 : Window
    {
        WebBrowserControl webBrowserControl1;
        

        protected override void SetupWindow(WindowCreationParameters prm)
        {
            prm.Left = 400;
            prm.Top = 150;
            prm.Width = 500;
            prm.Height = 600;
            prm.Title = "My 4th window";

        }

        protected override IEnumerable<Control> SetupControls()
        {
            webBrowserControl1 = new WebBrowserControl(10, 10, 480, 580)
            {
                LockLeft = true,
                LockRight = true,
                LockTop = true,
                LockBottom = true
            };
            webBrowserControl1.GotFocus += WebBrowserControl1_GotFocus;

            return new Control[]
            {
                webBrowserControl1
            };
        }

        
    }
}

