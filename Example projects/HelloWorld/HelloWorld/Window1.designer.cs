using System;
using AppKit;
using System.Collections.Generic;
using Einhugur;
using Einhugur.Forms;
using Einhugur.Arguments;
using Einhugur.Menus;

namespace HelloWorld
{
    public partial class Window1
    {
        Button button1;

        protected override void SetupWindow(WindowCreationParameters prm)
        {
			prm.Left = 200;
			prm.Top = 100;
			prm.Width = 500;
			prm.Height = 400;
			prm.Title = "My first window";
            prm.InitialPlacement = WindowPlacement.Center;
        }

        protected override IEnumerable<Control> SetupControls()
        {
            button1 = new Button(30, 50, 120, 23)
            {
                Caption = "Hello world"
            };
            button1.Pressed += Button1_Pressed;

            return new Control[]
            {
                button1
            };
        }

        
    }
}

