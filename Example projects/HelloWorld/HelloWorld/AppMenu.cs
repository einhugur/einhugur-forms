using AppKit;
using Einhugur.Menus;


namespace HelloWorld
{
    public class AppMenu : NSMenu
    {
        public static MenuItem QuitMenuItem;

        public AppMenu(string title) : base(title)
        {
            // Application top level Menu
            var appMenu = new NSMenuItem();
            appMenu.Submenu = new NSMenu();

            QuitMenuItem = new MenuItem("Quit", "q");

            appMenu.Submenu.Items = new []
            {
                new ApplicationServicesMenuItem("Services"),
                MenuItem.SeparatorItem,
                new ApplicationHideOthersMenuItem("Hide Others"),
                new ApplicationHideMenuItem("Hide"),
                new ApplicationShowAllMenuItem("Show All"),
                MenuItem.SeparatorItem,
                QuitMenuItem
            };

            // Edit top level menu
            var editMenu = new NSMenuItem();
            editMenu.Submenu = new NSMenu("Edit");

            editMenu.Submenu.Items = new []
            {
                new CutMenuItem("Cut"),
                new CopyMenuItem("Copy"),
                new PasteMenuItem("Paste"),
                new DeleteMenuItem("Delete"),
                MenuItem.SeparatorItem,
                new SelectAllMenuItem("Select all")
            };

            // Add all our top level menus
            this.Items = new[] {appMenu, editMenu };
        }

        

        
        
    }
}

