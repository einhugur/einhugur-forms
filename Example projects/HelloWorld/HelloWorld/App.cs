using System;
using AppKit;
using Foundation;
using Einhugur.Forms;
using System.Collections.Generic;
using Einhugur.Arguments;
using Einhugur.Menus;
using Einhugur.Dialogs;
using Einhugur;

namespace HelloWorld
{
    [Register("AppDelegate")]
    public class App : Application
    {
        protected override void Opening()
        {
            NSApplication.SharedApplication.MainMenu = new AppMenu("Test");

            MenuHandlers = new Dictionary<MenuItem, EventHandler<MenuEventArgs>>()
            {
                { AppMenu.QuitMenuItem,  QuitMenuItem_Action}
            };
        }

        protected override void Opened()
        {
            var window = new Window1();

            window.Show();
        }

        void QuitMenuItem_Action(object sender, EventArgs e)
        {
            Quit();
        }

       
        static void Main(string[] args)
        {
            Application.Init(typeof(App), args);    
        }
    }
}

