using System;
using AppKit;
using System.Collections.Generic;
using Einhugur;
using Einhugur.Forms;
using Einhugur.Arguments;
using Einhugur.Menus;

namespace ReusableContainerControl
{
    public partial class Window1
    {
        private Button button1;
        private NameControl nameControl;
        private Label lblName;
        private NameControl parentsName;
        private Label lblParentsName;

        protected override void SetupWindow(WindowCreationParameters prm)
        {
			prm.Left = 200;
			prm.Top = 100;
			prm.Width = 500;
			prm.Height = 400;
			prm.Title = "My first window";
            prm.InitialPlacement = WindowPlacement.Center;
        }

        protected override IEnumerable<Control> SetupControls()
        {
            lblName = new Label(1,12, 100, 23)
            {
                Caption = "Name:",
                LockTop = true,
                LockLeft = true,
                Alignment = TextAlignment.Right
            };

            nameControl = new NameControl(105, 8, 300, 24)
            {
                FirstName = "John",
                LastName = "Smith"
            };
            
            button1 = new Button(260, 60, 120, 23)
            {
                Caption = "Get full name"
            };
            button1.Pressed += Button1_Pressed;
            
            lblParentsName = new Label(1,132, 100, 23)
            {
                Caption = "Parent name:",
                LockTop = true,
                LockLeft = true,
                Alignment = TextAlignment.Right
            };

            parentsName = new NameControl(105, 128, 300, 24)
            {

            };

            return new Control[]
            {
                lblName,
                nameControl,
                button1,
                lblParentsName,
                parentsName
            };
        }

        
    }
}

