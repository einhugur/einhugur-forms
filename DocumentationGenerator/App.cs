﻿using System;
using Foundation;
using Einhugur.Forms;
using System.Collections.Generic;
using Einhugur.Arguments;
using Einhugur.Menus;
using Newtonsoft.Json;
using System.IO;
using Einhugur.Drawing;
using Einhugur;
using Einhugur.Threading;
using Einhugur.Dialogs;
using ErrorEventArgs = Einhugur.Arguments.ErrorEventArgs;

namespace DocGen
{
    [Register("AppDelegate")]
    public class App 
    {

        


        static void Main(string[] args)
        {
            Console.WriteLine("Generating intermediate documentation json file");
            var classes = new List<ClassInterfaceExtractor.FrameworkClass>();

            var extractor = new ClassInterfaceExtractor();
            extractor.ProcessClasses(new[]
            {
                typeof(Color),
                typeof(Font),
                typeof(GraphicsContext),
                typeof(Picture),
                typeof(Application),
                typeof(Button),
                typeof(Canvas),
                typeof(GroupBox),
                typeof(Checkbox),
                typeof(ContainerControl),
                typeof(Control),
                typeof(TabControl),
                typeof(Scrollbar),
                typeof(TabControl.Tab),
                typeof(Label),
                typeof(PopupButton),
                typeof(LevelIndicator),
                typeof(ProgressBar),
                typeof(WebBrowserControl),
                typeof(RadioButton),
                typeof(ListBox),
                typeof(TextField),
                typeof(SecureTextField),
                typeof(Window),
                typeof(MenuItem),
                typeof(SpecialMenuItem),
                typeof(AboutMenuItem),
                typeof(CutMenuItem),
                typeof(CopyMenuItem),
                typeof(PasteMenuItem),
                typeof(DeleteMenuItem),
                typeof(SelectAllMenuItem),
                typeof(ApplicationHideMenuItem),
                typeof(ApplicationHideOthersMenuItem),
                typeof(ApplicationServicesMenuItem),
                typeof(ApplicationShowAllMenuItem),
                typeof(Localized),
                typeof(Timer),
                typeof(MessageBox),
                typeof(OpenFileDialog),
                typeof(SaveFileDialog),
                typeof(SelectDirectoryDialog),
                typeof(ActionEventArgs),
                typeof(KeyEventArgs),
                typeof(CellEventArgs),
                typeof(MenuEventArgs),
                typeof(MouseEventArgs),
                typeof(MouseWheelEventArgs),
                typeof(PaintEventArgs),
                typeof(ErrorEventArgs),
                typeof(NavigationErrorEventArgs),
                typeof(UrlEventArgs),
                typeof(UrlPolicyEventArgs),
                typeof(Screen),
                typeof(FileSystemItem)
            }, classes, true);

            var json = JsonConvert.SerializeObject(classes.ToArray(), Formatting.Indented);

            var dir = FileSystemItem.CurrentDirectory;

            dir = dir.Parent.Parent.Parent.Parent.Parent;

            var docsDir = dir.Child("Docs");

            if (!docsDir.Exists)
            {
                docsDir.CreateDirectory();
            }

            var path = docsDir.Child("intermediate.json");

            /*var dir = Directory.GetCurrentDirectory();

            var dirInfo = Directory.GetParent(dir).Parent.Parent.Parent.Parent;

            if(!Directory.Exists(Path.Combine(dirInfo.ToString(), "Docs")))
            {
                Directory.CreateDirectory(Path.Combine(dirInfo.ToString(), "Docs"));
            }

            var path = Path.Combine(dirInfo.ToString(), "Docs", "intermediate.json");*/
            
            using (StreamWriter sw = new StreamWriter(path.FullPath))
            {
                sw.Write(json);
            }

               
        }
    }
}

